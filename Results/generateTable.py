import csv
with open("./result_0624_classification_binary.txt","r") as fileObj:
	stats = {}
	lines = fileObj.readlines()
	methodName = ""
	optionName = ""
	for index, line in enumerate(lines):
		line = line.strip()
		if line.startswith("classification:"):
			methodName = line.split(':')[-1].strip()
			stats[methodName]={}
		elif line.startswith("option"):
			optionName = line.split(':')[-1].strip()
			stats[methodName][optionName]=[]
		elif line.startswith("best roc_auc score"):
			stats[methodName][optionName].append(float(line.split(':')[-1].strip()))
		elif line.startswith("roc_auc of prediction"):
			stats[methodName][optionName].append(float(line.split(':')[-1].strip()))
			tablelineItem = lines[index + 4].strip().split()
			stats[methodName][optionName] += [float(s.strip()) for s in tablelineItem[1:4]]
			print methodName,optionName
			print stats
			
		
# print stats

methods = ['Medication Details', 'Confirm Disease Was Checked', 'Rule of N', 'Use Distinct Dates', 'Level of Evidence', 'Credentials of the Actor', 'Where Did It Happen?', 'Check For Negation']

options = ["BOW","BOW + Site", "BOW + Cond","BOW + CUI","BOW + St","Embedding","Embedding + Site","Embedding + Cond","Embedding + CUI","Embedding + ST"]
#options = ["baseline","site", "algorithm","CUI","st"]
with open("result_0624_classification_binary.csv","w") as outputFileObj:
	
	fp = csv.writer(outputFileObj,delimiter=',')

	fp.writerow(["ScoreType","Option"]+methods)
	scoreCategory = ["best roc_auc score for cross_validation","roc_auc of prediction","precision","recall","f1"]
	for i in xrange(len(scoreCategory)):
		for op in options:
			row = [scoreCategory[i],op]
			for m in methods:
				row.append(stats[m][op][i])
				print m
			fp.writerow(row)
		fp.writerow('\n')
	fp.writerow('\n')

