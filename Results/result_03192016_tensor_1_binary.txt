whole size 163
feature matrix dimension: (114, 10)
option: tensor
best roc_auc score: 0.5
roc_auc of prediction: 0.500000
             precision    recall  f1-score   support

          0       0.92      1.00      0.96        45
          1       0.00      0.00      0.00         4

avg / total       0.84      0.92      0.88        49

feature matrix dimension: (114, 20)
option: tensor
best roc_auc score: 0.667335004177
roc_auc of prediction: 0.800000
             precision    recall  f1-score   support

          0       0.93      0.87      0.90        45
          1       0.14      0.25      0.18         4

avg / total       0.86      0.82      0.84        49

feature matrix dimension: (114, 30)
option: tensor
best roc_auc score: 0.553682818157
roc_auc of prediction: 0.377778
             precision    recall  f1-score   support

          0       0.88      0.49      0.63        45
          1       0.04      0.25      0.07         4

avg / total       0.81      0.47      0.58        49

feature matrix dimension: (114, 40)
option: tensor
best roc_auc score: 0.556648565859
roc_auc of prediction: 0.311111
             precision    recall  f1-score   support

          0       0.91      0.89      0.90        45
          1       0.00      0.00      0.00         4

avg / total       0.83      0.82      0.83        49

feature matrix dimension: (114, 50)
option: tensor
best roc_auc score: 0.976573377889
roc_auc of prediction: 0.494444
             precision    recall  f1-score   support

          0       0.93      0.87      0.90        45
          1       0.14      0.25      0.18         4

avg / total       0.86      0.82      0.84        49

feature matrix dimension: (114, 60)
option: tensor
best roc_auc score: 0.888554720134
roc_auc of prediction: 0.527778
             precision    recall  f1-score   support

          0       0.92      0.80      0.86        45
          1       0.10      0.25      0.14         4

avg / total       0.86      0.76      0.80        49

feature matrix dimension: (114, 100)
option: tensor
best roc_auc score: 0.96365914787
roc_auc of prediction: 0.638889
             precision    recall  f1-score   support

          0       0.95      0.87      0.91        45
          1       0.25      0.50      0.33         4

avg / total       0.89      0.84      0.86        49

feature matrix dimension: (114, 200)
option: tensor
best roc_auc score: 0.857546644389
roc_auc of prediction: 0.422222
             precision    recall  f1-score   support

          0       0.91      0.91      0.91        45
          1       0.00      0.00      0.00         4

avg / total       0.84      0.84      0.84        49

feature matrix dimension: (114, 300)
option: tensor
best roc_auc score: 0.881209969368
roc_auc of prediction: 0.300000
             precision    recall  f1-score   support

          0       0.93      0.84      0.88        45
          1       0.12      0.25      0.17         4

avg / total       0.86      0.80      0.83        49

feature matrix dimension: (114, 400)
option: tensor
best roc_auc score: 0.901468950153
roc_auc of prediction: 0.683333
             precision    recall  f1-score   support

          0       0.92      0.78      0.84        45
          1       0.09      0.25      0.13         4

avg / total       0.85      0.73      0.79        49

