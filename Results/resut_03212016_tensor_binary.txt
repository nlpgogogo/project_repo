whole size 163
feature matrix dimension: (114, 10)
option: tensor
best roc_auc score: 0.562057922584
roc_auc of prediction: 0.150000
             precision    recall  f1-score   support

          0       0.87      0.58      0.69        45
          1       0.00      0.00      0.00         4

avg / total       0.80      0.53      0.64        49

feature matrix dimension: (114, 20)
option: tensor
best roc_auc score: 0.882901698691
roc_auc of prediction: 0.966667
             precision    recall  f1-score   support

          0       0.96      0.98      0.97        45
          1       0.67      0.50      0.57         4

avg / total       0.93      0.94      0.93        49

feature matrix dimension: (114, 30)
option: tensor
best roc_auc score: 0.913192703982
roc_auc of prediction: 0.688889
             precision    recall  f1-score   support

          0       0.91      0.93      0.92        45
          1       0.00      0.00      0.00         4

avg / total       0.84      0.86      0.85        49

feature matrix dimension: (114, 40)
option: tensor
best roc_auc score: 0.791722361459
roc_auc of prediction: 0.666667
             precision    recall  f1-score   support

          0       0.91      0.93      0.92        45
          1       0.00      0.00      0.00         4

avg / total       0.84      0.86      0.85        49

feature matrix dimension: (114, 50)
option: tensor
best roc_auc score: 0.798593706488
roc_auc of prediction: 0.672222
             precision    recall  f1-score   support

          0       0.95      0.91      0.93        45
          1       0.33      0.50      0.40         4

avg / total       0.90      0.88      0.89        49

feature matrix dimension: (114, 100)
option: tensor
best roc_auc score: 0.849018379282
roc_auc of prediction: 0.550000
             precision    recall  f1-score   support

          0       0.93      0.84      0.88        45
          1       0.12      0.25      0.17         4

avg / total       0.86      0.80      0.83        49

feature matrix dimension: (114, 200)
option: tensor
best roc_auc score: 0.920258980785
roc_auc of prediction: 0.538889
             precision    recall  f1-score   support

          0       0.91      0.89      0.90        45
          1       0.00      0.00      0.00         4

avg / total       0.83      0.82      0.83        49

feature matrix dimension: (114, 300)
option: tensor
best roc_auc score: 0.901468950153
roc_auc of prediction: 0.666667
             precision    recall  f1-score   support

          0       0.92      0.80      0.86        45
          1       0.10      0.25      0.14         4

avg / total       0.86      0.76      0.80        49

