import csv
with open("result_03312016_tensor_cp_med_tfidf.txt","r") as fileObj:
	stats = {}
	lines = fileObj.readlines()
	methodName = ""
	for index, line in enumerate(lines):		
		line = line.strip()
		if line.startswith("cp rank:"):
			methodName = line[:]
			print methodName
			stats[methodName]=[]
		elif line.startswith("best roc_auc score"):
			a = float(line.split(':')[-1].strip())
			stats[methodName].append(a)
		elif line.startswith("roc_auc of prediction"):
			stats[methodName].append(float(line.split(':')[-1].strip()))
			tablelineItem = lines[index + 4].strip().split()
			stats[methodName] += [float(s.strip()) for s in tablelineItem[1:4]]
	print stats


with open("result_03312016_tensor_cp_med_tfidf.csv","w") as outputFileObj:
	
	fp = csv.writer(outputFileObj,delimiter=',')
	scoreCategory = ["best roc_auc score for cross_validation","roc_auc of prediction","precision","recall","f1"]
	fp.writerow(["matrix dimension"]+scoreCategory)
	#scoreCategory = ["roc_auc of prediction","precision","recall","f1"]
	#scoreCategory = ["best roc_auc score for cross_validation","roc_auc of prediction","precision","recall","f1"]
	for i in range(len(stats.keys())):
		
		row = [stats.keys()[i]]
		
		row = row + stats[stats.keys()[i]]
		print row
		fp.writerow(row)