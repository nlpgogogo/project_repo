select count-distinct-dt(records)
Exclude non-clinical providers
Case contains more than two ICD-9 codes and at least one medication from Case criteria
Select subjects having two or more Drusen diagnoses (see 6a above) within ophthalmology/optometry (see 7a above), where the earliest and most recent diagnosis are at least fourteen days apart.
keywords that are not negated
Diagnosis of MRSA was made in the outpatient setting
No billing codes for hypothyroidism, no evidence of thyroid replacement meds
Must contain at least two Past Medical History sections and Medication lists (could substitute two non-acute clinic visits or requirement for annual physical)
Fulfilled lab results requirement for AKI criteria
family history related terms (50 characters prior to 50 characters after)
(more than 2 occurrences of any of the below)
strings (for text searching with SQL or other systems, page 21) - must be probable or asserted and not in a Family Medical History section) indicative of:
Culture drawn in the outpatient (including emergency department) setting
Case contains more than two ICD-9 codes and at least one medication from Case criteria 
relevant ICD9 code for Atopic Dermatitis (691.8) in three or more in person visits, on separate calendar days
Has >=4 meds simultaneous* med classes below mentioned on at least 2 occasions  1 month apart (does not have to be the same med classes in each of the 2 occasions)
Note:  must screen for ulcerative colitis
Determine HDL's during hospital stays
where the earliest and most recent diagnosis are at least fourteen days apart
Patients on ACEI (List 1) with two mentions in medication records for at least 6 months
medical record includes two or more prescriptions for GERD -related medications 
Will exclude all but negated terms (e.g., exclude those with possible, probable, or asserted bundle branch blocks).  Should also exclude normalization negations like LBBB no longer present.
Select subjects having two or more AMD diagnoses 
2 or more consecutive INR measurements that are between 2 and 3 (measurements must be separated by at least one day)
3) has at least one hospital admission with a prior exposure to antibiotics 
>=2 occurrences of the following codes [ ICD9 CODE LIST]
Has diagnosis codes on 2+ separate calendar days for HIV infection (ICD 042.xx?044.xx).
of one anotherwho have ever had at least 2 Serum Creatinine (SCr) values within 7 days
Patients on ACEI (List 1) with two mentions in medication records for at least 6 months
At least one non negated mention from Table A, Column 1 (Disorder related mentions) and  Table A, Column 2 (Anatomical site related mentions) either in the SAME or adjacent  sentences in a section of interest
The code picks up all the patients who have the above mentioned terms in the progress notes as long as they are not preceded or followed by ?negative/no/not/absence? (within 3 words for nephropathy, 2 words for congenital, apkd & sickle)
Statin type and dose will be identified
Peak ALP >= 2x ULN?
2 or more ICD9 codes on separate days
Evidence of clopidogrel use (dose information available) within 30 days of second event.
2 or more ICD9 codes on separate days
Has diagnosis codes on 2+ separate calendar days for HIV infection (ICD 042.xx?044.xx).
Exclude any values from blood drawn during an inpatient hospitalization, ER OR Urgent Care visit
has had one or more visits to the vascular clinic/lab/department that resulted in the following diagnosis
Only considering inpatients older than 1 year with discharge summary notes
[MEDICATION LIST] Will likely require a standard dosage following them to distinguish from lab tests when using NLP to identify
Any of the following CPT codes for Evaluation & Management (E&M) that were submitted by a provider in an Optometry or Ophthalmology department [CPT CODE LIST]
Any other value (or the absence of a value) is considered valid
there was no indicator of topical or cream in the flanking text
if t1dm-dx-dt-cnt(pt ) == 0
at least one eye exam within the past two years of the reference date.  
coronary heart disease (CHD): any CHD codes detected  2 times in different years;
eGFR <60 mL/min per 1?73 m2 for >3 months
patients must have 2 DISTINCT dates or more, that is not 2 diagnoses on the same day and not including null date values.
Where the medication has an associated dose, frequency, route or strength
Note:  Record must contain at least one ECG (without AF keywords)
Will exclude all but negated terms (e.g., exclude those with possible, probable, or asserted bundle branch blocks).  Should also exclude normalization negations like LBBB no longer present.
having at least one eye exam within the last two years if living or the last two years prior to date of death if deceased
provider must be a clinical provider
2 or more unique diagnosis dates 
Reported History of Appendicitis
For orally administered medications that may affect RBC indices, we populated a list of the commonly used medications using both generic and brand names 
where the earliest and most recent diagnosis are at least fourteen days apart
For subjects having no glaucoma diagnoses (see 4a above), select subjects having no intraocular pressure lowering medications where route of administration is ophthalmic [ MEDICATIONS ]
Kidney damage for >3 months (albuminuria is the most common marker of kidney damage and is also associated with rapid progression)
Have 2 notes (dosing information) from the Coumadin clinic (pharmacy) or the cardiology Coumadin clinic between 3 and 12 weeks apart 
two or more in-person visits, on separate calendar days.
Two or more encounters, on separate calendar days
Keep HDL's that were not obtained during a hospital stay
Determine if subject taking 500 mg or more per day
If such use of chemotherapeutic medications was detected, all later samples were excluded. If no chemotherapy was administered during the aforementioned period of time, the patients were considered to be free of disease and later samples were included in 
To define a 'definite' PAD case population
at least 2 Hypertension diagnoses with ICD-9 code 401.xx that are not on the same day
Select subjects having two or more Drusen diagnoses (see 6a above) within ophthalmology/optometry (see 7a above), where the earliest and most recent diagnosis are at least fourteen days apart.
Has two outpatient (if possible) measurements of SBP > 140 or DBP > 90 at least one month after meeting medication criteria while still on 3 simultaneous med classes
Smoking history?as defined by packs/day x years use?should, be captured 
Consider chronicity
relevant ICD9 code for Atopic Dermatitis (691.8) in two or more in person visits, on separate calendar days.
of one anotherwho have ever had at least 2 Serum Creatinine (SCr) values within 7 days
relevant ICD9 code for GERD (530.81 or 530.11) in two or more in person visits, on separate calendar days
within 72 hours of admission as an inpatient
Select subjects having two or more AMD diagnoses (see 2a above); within ophthalmology/optometry (see 3a above).
contains more than two ICD 9 codes for Ulcerative Colitis (556*)
Select from subjects having Drusen ICD9 codes given by ophthalmologist/optometrist within ophthalmology/optometry department(s).
Select from subjects having AMD ICD9 codes given by ophthalmologist/optometrist within ophthalmology/optometry department(s). 
two or more in-person visits, on separate calendar days.
relevant ICD9 code GERD (530.81) in three or more in person visits, on separate calendar days
410.*  Code must occur on two successive days
2 or more consecutive INR measurements that are between 2 and 3 (measurements must be separated by at least one day)
Individuals were excluded if they did not have HDL results for at least two days
Physician entered diagnosis  A physician entered diagnosis code is one that is derived from encounter or problem list sources only (excludes billing codes).
No antecedent inpatient hospitalization in the prior 1 year before MRSA infection
codes indicating the absence of diabetic retinopathy and/or macular edema may be used 
Select subjects having two or more AMD diagnoses (see 2a above); within ophthalmology/optometry (see 3a above).
Have 2 notes (dosing information) from the Coumadin clinic (pharmacy) or the cardiology Coumadin clinic between 3 and 12 weeks apart 
Samples collected during an inpatient hospitalization (admit date  collection date  discharge date) should be excluded unless this sample was the only one available for a patient
Identify if subject has adrenal steroids on 2 or more unique dates
?absent? or ?negative? or other negative mention for controls
relevant ICD9 code for Atopic Dermatitis (691.8) in three or more in person visits, on separate calendar days
Positive mention is defined using ConText for assigning statuses to each NLP result ? positive, probable, and negative
will proceed to boxes urine volume production during a 12-hour shift
relevant ICD9 code for Atopic Dermatitis (691.8) in two or more in person visits, on separate calendar days.
Must have two or more in person visits in the last 5 years
first random glucose date, first HbA1c date, the 2nd qualifying fasting glucose date where glucose >= 126, or the first diabetes medication date
Individual?s medical record includes two or more prescriptions Atopic Dermatitis-related medications
Select subjects having two or more Drusen diagnoses (see 6a above) within ophthalmology/optometry (see 7a above), where the earliest and most recent diagnosis are at least fourteen days apart.
on same date as at least one outpatient CPT below
2 Abnormal SBP or 2 abnormal DBP lab tests 6 months apart
Select subjects who have had an eye exam
having at least 2 diagnosis codes
When a cataract is found, search for a MedLEE attribute of certainty
of one anotherwho have ever had at least 2 Serum Creatinine (SCr) values within 7 days
patients must have 2 DISTINCT dates or more, that is not 2 diagnoses on the same day and not including null date values.
relevant ICD9 code GERD (530.81) in three or more in person visits, on separate calendar days
3 words).long as they are not preceded or followed by ?negative/no/not/absence? (within
At least on [sic] diagnosis code for C. diff (see above) and at least one affirmative mention of C. diff infection (unqualified by negation, uncertainty, or historical reference) in progress notes.
Must have at least a problem list and/or note containing non-empty (can say none) medication list and past medical history before or immediately after the time of the ECG.  
 Any encounter with a provider in an Optometry or Ophthalmology department
Require at least 2 instances of either medication or lab (a combination is acceptable) with at least 3 months between the first and last instance of medication or lab
