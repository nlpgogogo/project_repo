import numpy as np
import pickle
import cPickle
from collections import defaultdict
import sys, re
import pandas as pd

def build_data_cv(data_folder, cv=10, clean_string=True):
    """
    Loads data and split into 10 folds.
    """
    revs = []
    pos_file = data_folder[0]
    neg_file = data_folder[1]
    pos_test_file = data_folder[2]
    neg_test_file = data_folder[3]
    vocab = defaultdict(float)
    with open(pos_file, "rb") as f:
        for line in f:       
            rev = []
            rev.append(line.strip())
            if clean_string:
                orig_rev = clean_str(" ".join(rev))
            else:
                orig_rev = " ".join(rev).lower()
            words = set(orig_rev.split())
            for word in words:
                vocab[word] += 1

            datum  = {"y":1,
                      "train":"train",   
                      "text": orig_rev,                             
                      "num_words": len(orig_rev.split()),
                      "split": np.random.randint(0,cv)}
            revs.append(datum)
    with open(neg_file, "rb") as f:
        for line in f:       
            rev = []
            rev.append(line.strip())
            if clean_string:
                orig_rev = clean_str(" ".join(rev))
            else:
                orig_rev = " ".join(rev).lower()
            words = set(orig_rev.split())
            for word in words:
                vocab[word] += 1
            datum  = {"y":0, 
                      "train":"train", 
                      "text": orig_rev,                             
                      "num_words": len(orig_rev.split()),
                      "split": np.random.randint(0,cv)}
            revs.append(datum)
    with open(pos_test_file, "rb") as f:
        for line in f:       
            rev = []
            rev.append(line.strip())
            if clean_string:
                orig_rev = clean_str(" ".join(rev))
            else:
                orig_rev = " ".join(rev).lower()
            words = set(orig_rev.split())
            for word in words:
                vocab[word] += 1
            datum  = {"y":1, 
                      "train":"test",
                      "text": orig_rev,                             
                      "num_words": len(orig_rev.split()),
                      "split": np.random.randint(0,cv)}
            revs.append(datum)
    with open(neg_test_file, "rb") as f:
        for line in f:       
            rev = []
            rev.append(line.strip())
            if clean_string:
                orig_rev = clean_str(" ".join(rev))
            else:
                orig_rev = " ".join(rev).lower()
            words = set(orig_rev.split())
            for word in words:
                vocab[word] += 1
            datum  = {"y":0, 
                      "train":"test", 
                      "text": orig_rev,                             
                      "num_words": len(orig_rev.split()),
                      "split": np.random.randint(0,cv)}
            revs.append(datum)
    #print revs
    #print vocab
    return revs, vocab

    
def get_W(word_vecs, k):
    """
    Get word matrix. W[i] is the vector for word indexed by i
    """
    vocab_size = len(word_vecs)
    word_idx_map = dict()
    W = np.zeros(shape=(vocab_size+1, k), dtype='float32')            
    W[0] = np.zeros(k, dtype='float32')
    i = 1
    for word in word_vecs:
        W[i] = word_vecs[word]
        word_idx_map[word] = i
        i += 1
    #print W
    #print W.shape
    #print word_idx_map
    return W, word_idx_map

def load_bin_vec(fname, vocab):
    """
    Loads 300x1 word vecs from Google (Mikolov) word2vec
    """
    word_vecs = {}
    f = open(fname, 'rb')
    a = pickle.load(f)
    (mem, hwoov, hwid) = a
    f.close()
    for word in hwid.keys():
        word_vecs[word] = mem[hwid[word]]
    #print word_vecs
    return word_vecs

def add_unknown_words(word_vecs, vocab, k, min_df=1):
    """
    For words that occur in at least min_df documents, create a separate word vector.    
    0.25 is chosen so the unknown vectors have (approximately) same variance as pre-trained ones
    """
    ws = []
    word_dict = {}
    for word in vocab:
        if word not in word_vecs and vocab[word] >= min_df:
            #print word
            word_dict[word] = np.random.uniform(-0.25,0.25,k)  
            ws.append(word)
        else:
            word_dict[word] = word_vecs[word]
    print "number of words added random word vector",len(ws)
    return word_dict

def clean_str(string, TREC=False):
    """
    Tokenization/string cleaning for all datasets except for SST.
    Every dataset is lower cased except for TREC
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)  
    string = re.sub("[/*'-]", " ",string)   
    string = re.sub(r"\'s", " \'s", string) 
    string = re.sub(r"\'ve", " \'ve", string) 
    string = re.sub(r"n\'t", " n\'t", string) 
    string = re.sub(r"\'re", " \'re", string) 
    string = re.sub(r"\'d", " \'d", string) 
    string = re.sub(r"\'ll", " \'ll", string) 
    string = re.sub(r",", " , ", string) 
    string = re.sub(r"!", " ! ", string) 
    string = re.sub(r"\(", " \( ", string) 
    string = re.sub(r"\)", " \) ", string) 
    string = re.sub(r"\?", " \? ", string) 
    string = re.sub(r"\s{2,}", " ", string)    
    return string.strip() if TREC else string.strip().lower()

def clean_str_sst(string):
    """
    Tokenization/string cleaning for the SST dataset
    """
    string = re.sub(r"[^A-Za-z0-9(),!?\'\`]", " ", string)   
    string = re.sub(r"\s{2,}", " ", string)    
    return string.strip().lower()

def check_balance(revs):
    cv_split = []
    for i in revs:
        if i['y'] == 1:
            cv_split.append(i['split'])
    return len(list(set(cv_split)))

if __name__=="__main__":    
    w2v_file = sys.argv[1]     #read train pos sent, train neg sent, test pos sent and test neg sent separately
    pos = sys.argv[2]
    neg = sys.argv[3]
    pos_test = sys.argv[4]
    neg_test = sys.argv[5]
    data_folder = [pos,neg,pos_test,neg_test]    
    print "loading data...",        
    revs, vocab = build_data_cv(data_folder, cv=3, clean_string=True)

    if check_balance(revs)!= 3:
        revs, vocab = build_data_cv(data_folder, cv=3, clean_string=True)
    print "number of class in train", check_balance(revs)
    max_l = np.max(pd.DataFrame(revs)["num_words"])
    print "data loaded!"
    print "number of sentences: " + str(len(revs))
    print "vocab size: " + str(len(vocab))
    print "max sentence length: " + str(max_l)
    print "loading word2vec vectors...",
    w2v = load_bin_vec(w2v_file, vocab)
    print "word2vec loaded!"
    print "num words already in word2vec: " + str(len(w2v))
    print set(w2v.keys())-set(vocab.keys())
    print set(vocab.keys())-set(w2v.keys())
    k = len(w2v.values()[1])
    w2v = add_unknown_words(w2v, vocab, k)
    W, word_idx_map = get_W(w2v,k)
    print W.shape
    rand_vecs = {}
    rand_vecs = add_unknown_words(rand_vecs, vocab, k)
    print "lenvocab2",len(vocab)
    W2, _ = get_W(rand_vecs,k)
    
    print W2.shape
    cPickle.dump([revs, W, W2, word_idx_map, vocab], open(sys.argv[6], "wb"))
    print "dataset created!"

