permutation <-function(s,mc){
  c_perm = c()
  for (i in 1:length(mc)){
    a = mc[i]
    f_perm = perm[s][[1]]
    c_perm = c(c_perm, f_perm[a])
  }
  return (c_perm)
}

# this generates raw confusion matrix without permutation
confMat <- function(gt, mc) {
  ## assumes gt and mc are in vector format
  ## columns: predicted as, rows: actual
  ni = length(gt)
  nc = length(unique(gt))
  nimc = length(mc)
  ncmc = length(unique(mc))
  #stopifnot(nimc == ni & ncmc == nc)
  cm = matrix(0, nc, nc) ## gt as rows
  for (i in 1:nc) {
    for (j in 1:nc) {
      cm[i,j] = sum(gt==i & mc==j)
    }
  }
  return (cm)
}

# this function greedily reorders cluster alignment
clusterAnalysis <- function(mc, gt) {
  #source('../../r_util/confMat.R')
  ## c is cluster
  ## gt is ground truth
  cm = confMat(gt, mc)
  ngt = dim(cm)[1]
  nc = dim(cm)[2]
  
  ## greedy reordering
  if (nc==ngt) {
    for (i in 1:nc){
      imax = which.max(cm[i,i:nc])+i-1
      tv = cm[,i]
      cm[,i] = cm[,imax]
      cm[,imax] = tv
    }
  }
  return (cm)
}

# this function computes precision, recall, and f-measure
cmPRF <- function(cm) {
  ## calculate precision, recall and f-measure given result output
  nc = dim(cm)[1]; nc2 = dim(cm)[2]
  stopifnot(nc==nc2)
  pres = rep(0, nc); recs = rep(0, nc); f1s = rep(0, nc)
  tp_whole = 0
  fp_whole = 0
  fn_whole = 0
  for (c in 1:nc) {
    tp = cm[c,c]
    fn = sum( cm[c, -c] )
    fp = sum( cm[-c, c] ) 
    if(tp+fp == 0) {pre = 1} else {pre = tp / (tp+fp)}
    if(tp+fn == 0) {rec = 1} else {rec = tp / (tp+fn)}
    if(pre+rec == 0) {f = 0} else {f = 2*pre*rec / (pre+rec)}
    pres[c] = pre; recs[c] = rec; f1s[c] = f
    tp_whole = tp_whole + tp
    fp_whole = fp_whole + fp
    fn_whole = fn_whole + fn
  }
  
  micro_pre = tp_whole / (tp_whole+fp_whole)
  micro_recall = tp_whole / (tp_whole+fn_whole)
  micro_f1 = 2 * micro_pre * micro_recall / (micro_pre + micro_recall)
  res = list(pres=pres, recs=recs, f1s=f1s)
  avgrecall = mean(recs)
  avgprec = mean(pres)
  macro_f1 = 2 * avgprec * avgrecall / (avgprec + avgrecall)
  #return (macro_f1)
  res = list(pres=pres, recs=recs, f1s=f1s, micro_f1 = micro_f1, macro_f1 = macro_f1)
  return (res)
}



setwd("~/Dropbox/phenotype_patterns_data/src")
ls()
cluster = read.table("./cluster/cluster_0624_binary.txt",header = FALSE)
dim(cluster)
library(combinat)
gt = cluster[1,]
gt = gt+1
gt = unlist(gt, use.names = FALSE)
nc = length(unique(mc))
perm = permn(8)

List_pres <- list()
List_recall <- list()
List_f1 <- list()
max_macro = c()
max_micro = c()
for(ss in 2:dim(cluster)[1]){
  print(ss)
  mc = cluster[ss,]
  mc = mc+1
  mc = unlist(mc, use.names = FALSE)

  macro_f1 = c()
  micro_f1 = c()
  for(tt in 1:length(perm)){
    c_per = permutation(tt,mc)
    cm = confMat(gt, c_per)
    #cm = clusterAnalysis(c_per, gt)
    macro_f1 = c(macro_f1,cmPRF(cm)$macro_f1)
    micro_f1 = c(micro_f1,cmPRF(cm)$micro_f1)
    if(tt%%10000 == 0){
      print(tt)
    }
  }
  
  c_per_max = permutation(which.max(macro_f1),mc)
  print(which.max(macro_f1))
  print(macro_f1[which.max(macro_f1)])
  max_macro = c(max_macro,macro_f1[which.max(macro_f1)])
  print(which.max(micro_f1))
  print(micro_f1[which.max(micro_f1)])
  max_micro = c(max_micro,micro_f1[which.max(micro_f1)])
  cm_max = confMat(gt, c_per_max)
  #cm_max = clusterAnalysis(c_per_max, gt)
  cmPRF_max = cmPRF(cm_max)
  temp_recall = cmPRF_max$recs
  List_recall[[ss]] = c(temp_recall,cmPRF_max$macro_f1,cmPRF_max$micro_f1)
  temp_pres = cmPRF_max$pres
  List_pres[[ss]] = c(temp_pres,cmPRF_max$macro_f1,cmPRF_max$micro_f1)
  temp_f1 = cmPRF_max$f1s
  List_f1[[ss]] = c(temp_f1,cmPRF_max$macro_f1,cmPRF_max$micro_f1)
  
}

choose_max <-function(table){
  t = NULL
  n = nrow(table)/10
  for(i in 1:n){
    number = which.max(table[c((1+(i-1)*10):(10+(i-1)*10)),9])
    print((1+(i-1)*10)+number)
    t = rbind(t,table[((i-1)*10)+number,])
  }
  return(t)
}

Matrix_recall = do.call(rbind, List_recall)
matrix_pres = do.call(rbind, List_pres)
matrix_f1 = do.call(rbind,List_f1)

name = c("Check For Negation","Confirm Disease Was Checked","Credentials of the Actor","Level of Evidence",
         "Medication Details","Rule of N",
         "Use Distinct Dates","Where Did It Happen","macro_f1","micro_f1")



colnames(Matrix_recall) = name
colnames(matrix_pres) = name
colnames(matrix_f1) = name
colnames(matrix) = name

write.csv(Matrix_recall,"../Results/result_recall_cluster_0624_binary.csv")
write.csv(matrix_pres,"../Results/result_pres_cluster_0624_binary.csv")
write.csv(matrix_f1,"../Results/result_f1_cluster_0624_binary.csv")


recall_max = choose_max(Matrix_recall)
dim(recall_max)
pres_max = choose_max(matrix_pres)
dim(pres_max)
f1_max = choose_max(matrix_f1)
dim(f1_max)
mm = B = matrix(rep(0,11), nrow = 1,ncol = 11)
matrix = rbind(pres_max,recall_max,f1_max)
write.csv(matrix,"../Results/result_cluster_0624_binary.csv")
