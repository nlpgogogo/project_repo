setwd("~/Dropbox/phenopat/src")
cluster = read.table("./cluster/cluster_0501.txt",header = FALSE)
dim(cluster)

gt = cluster[1,]
gt = gt+1
gt = unlist(gt, use.names = FALSE)
mc = cluster[6,]
mc = mc+1
mc = unlist(mc, use.names = FALSE)
nc = length(unique(mc))
perm = permn(8)

permutation <-function(s,mc){
  c_perm = c()
  for (i in 1:length(mc)){
    a = mc[i]
    f_perm = perm[s][[1]]
    c_perm = c(c_perm, f_perm[a])
  }
  return (c_perm)
}

# this generates raw confusion matrix without permutation
confMat <- function(gt, mc) {
  ## assumes gt and mc are in vector format
  ## columns: predicted as, rows: actual
  ni = length(gt)
  nc = length(unique(gt))
  nimc = length(mc)
  ncmc = length(unique(mc))
  stopifnot(nimc == ni & ncmc == nc)
  cm = matrix(0, nc, nc) ## gt as rows
  for (i in 1:nc) {
    for (j in 1:nc) {
      cm[i,j] = sum(gt==i & mc==j)
    }
  }
  return (cm)
}

# this function greedily reorders cluster alignment
clusterAnalysis <- function(mc, gt) {
  #source('../../r_util/confMat.R')
  ## c is cluster
  ## gt is ground truth
  cm = confMat(gt, mc)
  ngt = dim(cm)[1]
  nc = dim(cm)[2]
  
  ## greedy reordering
  if (nc==ngt) {
    for (i in 1:nc){
      imax = which.max(cm[i,i:nc])+i-1
      tv = cm[,i]
      cm[,i] = cm[,imax]
      cm[,imax] = tv
    }
  }
  return (cm)
}

# this function computes precision, recall, and f-measure
cmPRF <- function(cm) {
  ## calculate precision, recall and f-measure given result output
  nc = dim(cm)[1]; nc2 = dim(cm)[2]
  stopifnot(nc==nc2)
  pres = rep(0, nc); recs = rep(0, nc); f1s = rep(0, nc)
  tp_whole = 0
  fp_whole = 0
  fn_whole = 0
  for (c in 1:nc) {
    tp = cm[c,c]
    fn = sum( cm[c, -c] )
    fp = sum( cm[-c, c] ) 
    if(tp+fp == 0) {pre = 1} else {pre = tp / (tp+fp)}
    if(tp+fn == 0) {rec = 1} else {rec = tp / (tp+fn)}
    if(pre+rec == 0) {f = 0} else {f = 2*pre*rec / (pre+rec)}
    pres[c] = pre; recs[c] = rec; f1s[c] = f
    tp_whole = tp_whole + tp
    fp_whole = fp_whole + fp
    fn_whole = fn_whole + fn
  }
  micro_pre = tp_whole / (tp_whole+fp_whole)
  micro_recall = tp_whole / (tp_whole+fn_whole)
  micro_f1 = 2 * micro_pre * micro_recall / (micro_pre + micro_recall)
  res = list(pres=pres, recs=recs, f1s=f1s)
  avgrecall = mean(recs)
  avgprec = mean(pres)
  macro_f1 = 2 * avgprec * avgrecall / (avgprec + avgrecall)
  #return (macro_f1)
  return (micro_f1)
}

macro_f1 = c()
for(tt in 1:length(perm)){
  c_per = permutation(tt,mc)
  cm = clusterAnalysis(c_per, gt)
  macro_f1= c(macro_f1,cmPRF(cm))
}
max(macro_f1)
