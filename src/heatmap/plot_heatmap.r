library(gplots)
setwd("~/Documents/python/luo/phenopat/src/heatmap")
feature_tfidf = read.table("feature_matrix_sorted_tfidf.txt")
word = read.table("word_tfidf.txt",header = FALSE)
words = as.vector(word[,1])

feature_tfidf_trans = t(feature_tfidf)
label = feature_tfidf_trans[nrow(feature_tfidf_trans),]
feature_tfidf_trans = feature_tfidf_trans[,order(feature_tfidf_trans[nrow(feature_tfidf_trans),])]
tag = feature_tfidf_trans[nrow(feature_tfidf_trans),]


color = c(rep("black",length(which(tag == 0))),
          rep("coral",length(which(tag == 1))),rep("cyan",length(which(tag == 2))),
          rep("blue",length(which(tag == 3))),rep("brown1",length(which(tag == 4))),
          rep("blueviolet",length(which(tag == 5))),rep("chocolate1",length(which(tag == 6))),
          rep("darkslategrey",length(which(tag == 7))),rep("cornflowerblue",length(which(tag == 8))))


#colfunc1 <- colorRampPalette(c("black", "green", "red"))
#colfunc2 <- colorRampPalette(c("red", "yellow"))
#hmcols <- c(colfunc1(100), colfunc2(100))
pdf("tfidf3.pdf",width=25,height=70)
#colors = c(seq(-2,-1,length=100),seq(-1,2,length=100),seq(2,4,length=100))
my_palette <- colorRampPalette(c("white","black"))(n = 100)

heatmap.2(feature_tfidf_trans[1:(nrow(feature_tfidf_trans)-1),],
          dendrogram='row', Colv = FALSE, 
          #col = hmcols,
          scale = "row",
          trace='none',margin=c(10,40),ColSideColors=color,
          main = "Embedding Feature Matrix",cex.main=100,
          xlab = "label",key = TRUE,keysize=0.3,density.info = "none",
          labRow =words,cexRow = 1.8,srtRow = 30, 
          #breaks =  colors, 
          col = my_palette,symbreaks = FALSE,symkey = FALSE)
dev.off()

dim(feature_tfidf_trans)
text(x = seq_along(labs), y = -0.2, srt = 45, labels = labs, xpd = TRUE)