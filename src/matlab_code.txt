
#tensor factorization for tfidf matrix

filename = 'mem_04172016.txt'
delimiterIn = ' ';
mem = importdata(filename,delimiterIn)

filename = 'feature_matrix_test_binary_04172016.txt'
delimiterIn = ' ';
binary_test = importdata(filename,delimiterIn)

filename = 'feature_matrix_train_binary_04172016.txt'
delimiterIn = ' ';
binary_train = importdata(filename,delimiterIn)

filename = 'feature_matrix_train_tfidf_04172016.txt'
delimiterIn = ' ';
tfidf_train = importdata(filename,delimiterIn)

filename = 'feature_matrix_test_tfidf_04172016.txt'
delimiterIn = ' ';
tfidf_test = importdata(filename,delimiterIn)

#################################################################

#train dimension(112,355)
#test dimension(48,355)

#create tensor

train_tensor_tfidf = repmat(0, [355 200 112])
train_tensor_tfidf= tensor(train_tensor_tfidf,[355,200,112])
for i = 1: 355
for j = 1:112
if tfidf_train(j,i) ~= 0
train_tensor_tfidf(i,:,j) = tfidf_train(j,i)* mem(i,:);
else
train_tensor_tfidf(i,:,j) = 0;
end;
end;
end;

test_tensor_tfidf = repmat(0, [355 200 48])
test_tensor_tfidf = tensor(test_tensor_tfidf,[355,200,48])
for i = 1:355
for j = 1:48
if tfidf_test(j,i) ~= 0
test_tensor_tfidf(i,:,j) = tfidf_test(j,i)* mem(i,:);
else
test_tensor_tfidf(i,:,j) = 0;
end;
end;
end;

####################################################
#cp factorization

y = [10,20,30,40,50,60,70,80,90,100]
seed = [10,20,30,40,50,60,70,80,90,100]

for k = 1:length(y)for j = 1:length(seed)rng(seed(j))a= parafac_als(train_tensor_tfidf,y(k)); cp{j} =a.U{3};param{j} = a.lambdafilename = [ 'cp_04172016_train_tfidf' num2str(k) '_' num2str(j) '.txt']dlmwrite(filename,cp{j} ,delimiterIn)end;param_tfidf_train{k} = paramcp_tfidf_train{k} = cpend;for k = 1:length(y)for j = 1:length(seed)rng(seed(j))a= parafac_als(test_tensor_tfidf,y(k)); cp{j} =a.U{3};param{j} = a.lambdafilename = [ 'cp_04172016_test_tfidf' num2str(k) '_' num2str(j) '.txt']dlmwrite(filename,cp{j} ,delimiterIn)end;param_tfidf_test{k} = paramcp_tfidf_test{k} = cpend;


#tensor factorization for binary matrix

train_tensor_binary = repmat(0, [355 200 112])
train_tensor_binary= tensor(train_tensor_binary,[355,200,112])
for i = 1: 355
for j = 1:112
if binary_train(j,i) ~= 0
train_tensor_binary(i,:,j) = binary_train(j,i)* mem(i,:);
else
train_tensor_binary(i,:,j) = 0;
end;
end;
end;

test_tensor_binary = repmat(0, [355 200 48])
test_tensor_binary = tensor(test_tensor_binary,[355,200,48])
for i = 1:355
for j = 1:48
if binary_test(j,i) ~= 0
test_tensor_binary(i,:,j) = binary_test(j,i)* mem(i,:);
else
test_tensor_binary(i,:,j) = 0;
end;
end;
end;




for k = 1:length(y)for j = 1:length(seed)rng(seed(j))a= parafac_als(train_tensor_binary,y(k)); cp{j} =a.U{3};param{j} = a.lambdafilename = [ 'cp_04172016_train_binary' num2str(k) '_' num2str(j) '.txt']dlmwrite(filename,cp{j} ,delimiterIn)end;param_binary_train{k} = paramcp_binary_train{k} = cpend;for k = 1:length(y)for j = 1:length(seed)rng(seed(j))a= parafac_als(test_tensor_binary,y(k)); cp{j} =a.U{3};param{j} = a.lambdafilename = [ 'cp_04172016_test_binary' num2str(k) '_' num2str(j) '.txt']dlmwrite(filename,cp{j} ,delimiterIn)end;param_binary_test{k} = paramcp_binary_test{k} = cpend;







