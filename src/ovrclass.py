import os
import csv
import random
import string
import sklearn
import logging
import sys
from collections import Counter
import numpy as np
from pprint import pprint
from sklearn import cross_validation,datasets
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier,Perceptron,PassiveAggressiveClassifier,RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier,NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score
from sklearn.tree import DecisionTreeClassifier


def get_file_path(filename):
    currentdirpath = os.getcwd()
    file_path = os.path.join(os.getcwd(), filename)
    return file_path

def read_csv(filepath,i):
    name = []
    with open(filepath, "rU") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            if len(row) == 0:
                continue
            name.append(row[i])
    return name


#Lable binary    
def getPatternList(pattern, patternlist):
    for i in range(len(pattern)):
        if pattern[i] == "Medication Details":
            patternlist[0][i] = 1
        elif pattern[i] == 'Confirm Disease Was Checked':
            patternlist[1][i] = 1
        elif pattern[i] == 'Rule of N':
            patternlist[2][i] = 1
        elif pattern[i] == 'Use Distinct Dates':
            patternlist[3][i] = 1
        elif pattern[i] == 'Level of Evidence':
            patternlist[4][i] = 1
        elif pattern[i] == 'Credentials of the Actor':
            patternlist[5][i] = 1
        elif pattern[i] == 'Where Did It Happen?':
            patternlist[6][i] = 1
        elif pattern[i] == 'Check For Negation':
            patternlist[7][i] = 1
    return patternlist







def prediction(clf, parameter, i):
    print all_train_text[48:55]
    cv = StratifiedKFold(train_pattern[i], 3,test_size=0.3, random_state=0)
    from sklearn.cross_validation import StratifiedKFold
    print('*' * 80)
    print pattern[i], "classfication"
    print('*' * 80)
    print("Training: ")
    pprint(parameter)
    #fit classifier
    gs_clf = GridSearchCV(clf, parameter, n_jobs=-1,scoring ="roc_auc", cv = cv)
    gs_clf = gs_clf.fit(X_train, train_pattern[i])
    print gs_clf.grid_scores_
    best_parameters, score, _ = max(gs_clf.grid_scores_, key=lambda x: x[1])
    print("Best parameters set found on development set:")
    best_estimator = gs_clf.best_estimator_
    print best_estimator
    a = train_pattern[i]
    a = np.array(a)
    b = np.array(all_train_text)
    for train_index, test_index in cv:
        print test_index
        #cross validation
        text_train, text_test = X_train[train_index], X_train[test_index]
        y_train, y_test = a[train_index], a[test_index]
        #fit each fold and predict
        c = best_estimator.fit(text_train, y_train)
        pred = c.predict(text_test)
        print('_' * 50)
        y_train = y_train.tolist()
        print "train sentences:" +"\n"
        train = [i for i, j in enumerate(y_train) if j == 1]
        train_sen = [train_index[i] for i in train]
        for i in train_sen:
            print all_train_text[i]+'\n'
        word = {}
        coeff = c.coef_
        for k in range(0,len(coeff[0])):
            if coeff[0][k] != 0:
                word[features[k]] = coeff[0][k]
        print "rank feature in increasing order:"
        print sorted(word.items(), key=lambda x: x[1], reverse=True)
        print('_' * 50)
        print "pred",pred
        print "true",y_test
        y_test = y_test.tolist()
        print "test sentences:"
        test = [i for i, j in enumerate(y_test) if j == 1]
        test_sen = [test_index[i] for i in test]
        for i in test_sen:
            print all_train_text[i]+'\n'
        probas_ = c.decision_function(text_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        #print fpr,tpr
        roc_auc = auc(fpr, tpr)   
        mirco_precisionscore = precision_score(y_test,pred)
        micro_f1score = f1_score(y_test,pred)
        micro_recallscore = recall_score(y_test, pred)
        score = accuracy_score(y_test, pred)
        print('_' * 50)
        print("micro_f1_score: %f " %(micro_f1score))
        print("micro_precision_score: %f " %(mirco_precisionscore))
        print("micro_recall_score: %f " %(micro_recallscore))
        print("accuracy_score %f" %(score))
        print("roc_auc %f" %(roc_auc))
        print(classification_report(y_test, pred))
    return best_estimator



    

#read text file
filename = "/Users/zhongyi/Documents/python/luo/Analysis_R1R2_replaced.csv"
filename2 = "/Users/zhongyi/Documents/python/luo/CandidatePatterns.csv"
path = get_file_path(filename)
path2 = get_file_path(filename2)
print path
print path2

#read csv and put all text into list
all_text = read_csv(path, 2)#list
row_name = read_csv(path, 0)#list
Candidate_pattern= read_csv(path2, 0)#list
evidence_ID = read_csv(path2, 2)#list

#remove the pattern which has evidence less than 8
useful_pattern = []
useful_evidence = []
for j in range(0,len(evidence_ID)):
    evidence = evidence_ID[j].split(",")
    if len(evidence) > 8:
        useful_evidence.append(evidence_ID[j])
        useful_pattern.append(Candidate_pattern[j])

#match row_name with evidence
wholetext = []
wholepattern = []
for i in range(0, len(row_name)):
    for number_list in range(0,len(useful_evidence)):
        evidence = useful_evidence[number_list].split(",")
        for item in evidence:
            if str(row_name[i].strip()) == item.strip():
                wholetext.append(all_text[i])
                wholepattern.append(useful_pattern[number_list])
            else:
                continue
#make sure each pattern is 7/3 
patternlist = []
for i in range(0,len(wholepattern)):
    if wholepattern[i] not in patternlist:
        patternlist.append(wholepattern[i])
    else:
        continue

match = {}
for i in range(8):
    a = []
    match[patternlist[i]] = a
    for j in range(len(wholepattern)):
        if wholepattern[j] == patternlist[i]:
            a.append(j)
        




random.seed(1234)
all_train_pattern = []
all_train_text = []
all_test_pattern = []
all_test_text = []
for key in match:
    lista = random.sample(match[key],int(round(0.7*len(match[key]))))
    c_list = [x for x in match[key] if x not in lista]
    for i in lista:
        all_train_pattern.append(wholepattern[i])
        all_train_text.append(wholetext[i])
    for j in c_list:
        all_test_pattern.append(wholepattern[j])
        all_test_text.append(wholetext[j])
print ("train set number")
print len(all_train_pattern)
print ("test set number")
print len(all_test_pattern)

#token
#from nltk import word_tokenize          
#from nltk.stem import WordNetLemmatizer 
#class LemmaTokenizer(object):
#    def __init__(self):
#            self.wnl = WordNetLemmatizer()
#    def __call__(self, doc):
        #return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]

vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
                                 stop_words='english',
                                 #tokenizer=LemmaTokenizer()
                                 )#remove the stop words
X_train = vectorizer.fit_transform(all_train_text)
X_test = vectorizer.transform(all_test_text)
pattern = list(set(all_train_pattern))
print ("all patterns")
print pattern

#create binary label 
y_train = [[0 for x in range(len(all_train_pattern))] for x in range(8)]
y_test = [[0 for x in range(len(all_test_pattern))] for x in range(8)]
train_pattern = getPatternList(all_train_pattern,y_train)
test_pattern = getPatternList(all_test_pattern,y_test)



 #   prediction(clf_linearsvc,parameter_linearsvc,i)
clf_linearsvc = LinearSVC(dual=False, class_weight="balanced",penalty = "l1",random_state = 0)
C_range = list(10**x for x in range(-3,3))
parameter_linearsvc = {"C" : C_range}
features = vectorizer.get_feature_names()

prediction(clf_linearsvc,parameter_linearsvc,7)


#for i in range(8):
#    prediction(clf_linearsvc,parameter_linearsvc,i)


 