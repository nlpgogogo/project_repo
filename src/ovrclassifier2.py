
import csv,os
import random
import string
import logging
import sys
import re
import nltk
from nltk.stem.porter import *
from nltk.corpus import stopwords
from collections import Counter
import numpy as np
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import StratifiedKFold
from pprint import pprint
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
from sklearn.cross_validation import StratifiedShuffleSplit
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier,Perceptron,PassiveAggressiveClassifier,RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier,NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score
from sklearn.tree import DecisionTreeClassifier

print "print report for each fold of cross validation, remove punctuation"

def get_file_path(filename):
    currentdirpath = os.getcwd()
    file_path = os.path.join(os.getcwd(), filename)
    return file_path

def read_csv(filepath,i):
    name = []
    with open(filepath, "rU") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            if len(row) == 0:
                continue
            name.append(row[i])
    return name
#find the row which is 1
def attach(feature,list):
    index = [i for i, j in enumerate(list) if j == '1']
    text = [feature[i] for i in index] 
    return text

#Lable binary    
def getPatternList(pattern, patternlist):
    for i in range(len(pattern)):
        if pattern[i] == "Medication Details":
            patternlist[0][i] = 1
        elif pattern[i] == 'Confirm Disease Was Checked':
            patternlist[1][i] = 1
        elif pattern[i] == 'Rule of N':
            patternlist[2][i] = 1
        elif pattern[i] == 'Use Distinct Dates':
            patternlist[3][i] = 1
        elif pattern[i] == 'Level of Evidence':
            patternlist[4][i] = 1
        elif pattern[i] == 'Credentials of the Actor':
            patternlist[5][i] = 1
        elif pattern[i] == 'Where Did It Happen?':
            patternlist[6][i] = 1
        elif pattern[i] == 'Check For Negation':
            patternlist[7][i] = 1
    return patternlist


def categorize(x):
    x1, x_cate = np.unique(x,return_inverse = True)
    #x_cate = x_cate.reshape(len(x),1)
    return x_cate

def reshape(x):
    x1, x_cate = np.unique(x,return_inverse = True)
    x_cate = x_cate.reshape(len(x),1)
    return x_cate

#token
def delete_replication(k):
    pos = []
    neg = []
    pos_tag = []
    neg_tag = []
    pos_site = []
    pos_algo = []
    neg_site = []
    neg_algo = []
    for j in range(len(all_train_pattern)):
        if train_pattern[k][j] == 1:
            pos_tag.append(j)
            pos.append(all_train_text[j])
        else:
            neg_tag.append(j)
            neg.append(all_train_text[j])
    common = list(set(pos).intersection(neg))
    unique_index = [i for i, j in enumerate(neg) if j in common]
    unique_neg = [x for x in neg_tag if x not in unique_index]
    textmerge = [all_train_text[i] for i in pos_tag] + [all_train_text[i] for i in unique_neg]
    patternmerge = [train_pattern[k][i] for i in pos_tag] + [train_pattern[k][i] for i in unique_neg]
    sitemerge = [all_train_site[i] for i in pos_tag] + [all_train_site[i] for i in unique_neg]
    algomerge = [all_train_algo[i] for i in pos_tag] + [all_train_algo[i] for i in unique_neg]
    sitemerge = reshape(sitemerge)    
    algomerge = reshape(algomerge)
    return pos, patternmerge, textmerge, sitemerge, algomerge

def token(i):
    a =  nltk.word_tokenize(i)
    words = []
    for j in a:
        words.append(stemmer.stem(j))
    return words 


def words( TEXT ):
    train = []   
    for i in TEXT:
        letters_only = []
        letters = re.sub("[0-9]", "_", i) 
        letters = letters.translate(None, string.punctuation)
        words = letters.lower().split() 
        stops = set(stopwords.words("english"))
        meaningful_words = [w for w in words if not w in stops]  
        letters_only.append(meaningful_words)
        train.append(" ".join(meaningful_words))    
    return train

def check(i):
    if i == 1:
        print "positive"
    if i == 0 :
        print "negative"
 
def prediction(i, choice):
    pos, trainpattern, traintext, trainsite, trainalgo = delete_replication(i)
    train = words(traintext)
    tfs = tfidf.fit_transform(train)
    tfs_test = tfidf.transform(test_)
    tfs_train_array = tfs.toarray()
    tfs_test_array = tfs_test.toarray()
    if choice == "none":
        tfs_train = tfs
        features = tfidf.get_feature_names()
        tfs_test = tfs_test
    if choice == "site":
        tfs_train = np.hstack((tfs_train_array,trainsite))  
        features = tfidf.get_feature_names() + ["site"]  
        tfs_test = np.hstack((tfs_test_array, testsite))
    if choice == "algorithm":
        tfs_train = np.hstack((tfs_train_array,trainalgo))  
        features = tfidf.get_feature_names() + ["algorithm"]  
        tfs_test = np.hstack((tfs_test_array, testalgo))
    print('*' * 60)
    print name[i], "classfication"
    print ("training size: %d" %len(trainpattern)) 
    print "option", choice
    print('*' * 60)
    print("tunning parameter")+"\n"
    print("tuning parameter grid: ")
    pprint(parameter_linearsvc)
    print("\n")
    #tunning prarmeter
    cv = StratifiedKFold(trainpattern, 3, random_state=0)
    gs_clf = GridSearchCV(clf_linearsvc, parameter_linearsvc, n_jobs=-1,scoring ="roc_auc", cv = cv)
    gs_clf = gs_clf.fit(tfs_train, trainpattern)
    print("roc_auc score grid for parameter tuning:")
    print("\n")
    print gs_clf.grid_scores_
    print("\n")
    print("Best parameters set found on development set:")
    best_estimator = gs_clf.best_estimator_
    print(best_estimator)
    print('_' * 50)
    print "cross validation report:"
    print("\n")
    auc_list = []
    #fit with all train data using best estimator    
    for train_index, test_index in cv:
        a = np.array(trainpattern)
        text_train, text_test = tfs_train[train_index], tfs_train[test_index]
        y_train, y_test = a[train_index], a[test_index]
        #fit each fold and predict
        c = best_estimator.fit(text_train, y_train)
        pred = c.predict(text_test)
        y_test = y_test.tolist()
        test = [s for s, j in enumerate(y_test) if j == 1]
        for s in test:
            test_sen = test_index[s] 
            print traintext[test_sen]
            print "prediction:",pred[s]
        probas_ = c.decision_function(text_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        print fpr,tpr
        #print fpr,tpr
        roc_auc = auc(fpr, tpr) 
        auc_list.append(roc_auc) 
    print("\n")
    print "auc score for each fold of corss validation:",auc_list
    print ("best roc_auc score:"),gs_clf.best_score_
    print('_' * 50)
    print("training model")
    clf = best_estimator.fit(tfs_train,trainpattern)
    word = {}
    coeff = clf.coef_
    for k in range(0,len(coeff[0])):
        if coeff[0][k] != 0:
            word[features[k]] = coeff[0][k]
    print("\n")
    print "rank feature in decreasing order:"
    print sorted(word.items(), key=lambda x: x[1], reverse=True)[1:10]
    print "rank feature in increasing order:"
    print sorted(word.items(), key=lambda x: x[1])[1:10]
    #predict
    print('_' * 50)
    print("prediction")
    pred = clf.predict(tfs_test)
    print(classification_report(test_pattern[i], pred))   
    print ('\n'*6)

    return best_estimator   
    

#read text file
#read text file
filename = "/Users/zhongyi/Documents/python/luo/Analysis_R1R2_replaced.csv"
filename2 = "/Users/zhongyi/Documents/python/luo/CandidatePatterns.csv"
filename3 = "/Users/zhongyi/Documents/python/luo/Annotations.csv"#new annotation

#read stop list
stop = []
f = open("stoplist.txt")
stop = f.read().splitlines() 

#read csv and put all text into list
all_text = read_csv(filename, 2)#list
row_name = read_csv(filename, 0)#list
Candidate_pattern= read_csv(filename2, 0)#list
evidence_ID = read_csv(filename2, 2)#list
site = read_csv(filename, 5)
algorithm = read_csv(filename, 4)


#read new annotations

#read csv and put all text into list
new_text = read_csv(filename3, 1)#list
new_algo = read_csv(filename3,2)
new_site = read_csv(filename3,3)
whole = []
for i in range(4,12):
    whole.append(read_csv(filename3, i))


name = ["Medication Details","Confirm Disease Was Checked","Rule of N","Use Distinct Dates",
"Level of Evidence","Credentials of the Actor","Where Did It Happen?","Check For Negation"]
whole_new_text = []
whole_new_pattern = []
whole_new_site = []
whole_new_algo = []
for i in range(8):
    temp_text= attach(new_text,whole[i])
    temp_site = attach(new_site,whole[i])
    temp_algo = attach(new_algo,whole[i])
    temp_name = [name[i]]*len(temp_text)
    whole_new_text = whole_new_text+temp_text
    whole_new_algo = whole_new_algo+temp_algo
    whole_new_site = whole_new_site+temp_site
    whole_new_pattern = whole_new_pattern+temp_name


#remove the pattern which has evidence less than 8
useful_pattern = []
useful_evidence = []
for j in range(0,len(evidence_ID)):
    evidence = evidence_ID[j].split(",")
    if len(evidence) > 7:
        useful_evidence.append(evidence_ID[j])
        useful_pattern.append(Candidate_pattern[j])

#match row_name with evidence
wholetext = []
wholepattern = []
wholesite = []
wholealgo = []
for i in range(0, len(row_name)):
    for number_list in range(0,len(useful_evidence)):
        evidence = useful_evidence[number_list].split(",")
        for item in evidence:
            if str(row_name[i].strip()) == item.strip():
                wholetext.append(all_text[i])
                wholesite.append(site[i])
                wholealgo.append(algorithm[i])
                wholepattern.append(useful_pattern[number_list])
            else:
                continue
print len(wholetext)
print len(whole_new_text)
wholetext =  wholetext + whole_new_text
wholepattern = wholepattern + whole_new_pattern
wholesite = wholesite + whole_new_site
wholealgo = wholealgo + whole_new_algo

print len(wholetext)

#get the whole pattern
patternlist = list(set(wholepattern))

match = {}
for i in range(8):
    a = []
    match[patternlist[i]] = a
    for j in range(len(wholepattern)):
        if wholepattern[j] == patternlist[i]:
            a.append(j)

       
#split into train test
sss = StratifiedShuffleSplit(wholepattern, 1, test_size=0.3, random_state=0)
for train_index, test_index in sss:
    print type(train_index)
    all_train_pattern, all_test_pattern = np.array(wholepattern)[train_index], np.array(wholepattern)[test_index]
    all_train_text, all_test_text = np.array(wholetext)[train_index], np.array(wholetext)[test_index]
    all_train_site, all_test_site = np.array(wholesite)[train_index], np.array(wholesite)[test_index]
    all_train_algo, all_test_algo = np.array(wholealgo)[train_index], np.array(wholealgo)[test_index]



#categorize algorithem and site 
all_train_site = categorize(all_train_site)
testsite = reshape(all_test_site)
all_train_algo = categorize(all_train_algo)
testalgo = reshape(all_test_algo)

#write train text
# traintext = open("traintext.txt","w")
# for i in all_train_text:
    # print i
    # traintext.write(i+'\n')
# traintext.close()
# print len(all_train_text)

pattern = list(set(all_train_pattern))
print ("all patterns")
print pattern

#create binary label 
y_train = [[0 for x in range(len(all_train_pattern))] for x in range(8)]
y_test = [[0 for x in range(len(all_test_pattern))] for x in range(8)]
train_pattern = getPatternList(all_train_pattern,y_train)
test_pattern = getPatternList(all_test_pattern,y_test)

test_ = words(all_test_text)
stemmer = PorterStemmer()
tfidf = TfidfVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore")




clf_linearsvc = LinearSVC(dual=False, class_weight="balanced",random_state = 0)
C_range = list(10**x for x in range(-3,3))
parameter_linearsvc = {"C" : C_range,
                        "penalty":["l2","l1"]}

#tfs = tfidf.fit_transform(all_train_text)
#print all_train_text

#vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
                                 #stop_words='english',decode_error ="ignore")


#X_train = vectorizer.fit_transform(all_train_text)


for i in range(8):
    prediction(i,"none")
    #prediction(i,"site")
    #prediction(i,"algorithm")
    print ("\n"*12)

#prediction(0,"none")
