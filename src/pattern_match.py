import os
import csv
import random
import string
import sklearn
import logging
import sys
import numpy as np
from sklearn import cross_validation,datasets
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier,Perceptron,PassiveAggressiveClassifier,RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier,NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score
from sklearn.tree import DecisionTreeClassifier

def get_file_path(filename):
	currentdirpath = os.getcwd()
	file_path = os.path.join(os.getcwd(), filename)
	return file_path

def read_csv(filepath,i):
	name = []
	with open(filepath, "rU") as csvfile:
		reader = csv.reader(csvfile)
		next(reader, None)
		for row in reader:
			if len(row) == 0:
				continue
			name.append(row[i])
	return name
#read text file
filename = "Analysis_R1R2_replaced.csv"
filename2 = "CandidatePatterns.csv"

path = get_file_path(filename)
path2 = get_file_path(filename2)
print path
print path2

#read csv and put all text into list
all_text = read_csv(path, 2)#list
row_name = read_csv(path, 0)#list
Candidate_pattern= read_csv(path2, 0)#list
evidence_ID = read_csv(path2, 2)#list

#remove the pattern which has evidence less than 8
useful_pattern = []
useful_evidence = []
for j in range(0,len(evidence_ID)):
    evidence = evidence_ID[j].split(",")
    if len(evidence) > 8:
        useful_evidence.append(evidence_ID[j])
        useful_pattern.append(Candidate_pattern[j])
print useful_evidence


#match row_name with evidence
for i in useful_evidence:
    i = map(str,i.split(","))
    print i
    index = [k for k, j in enumerate(row_name) if j in i ]
    print index


wholetext = []
wholepattern = []
print row_name

for i in range(len(row_name)):
    for number_list in range(len(useful_evidence)):
        evidence = useful_evidence[number_list].split(",")
        for item in evidence:
            if str(row_name[i].strip()) == item.strip():
                wholetext.append(all_text[i])
                wholepattern.append(useful_pattern[number_list])
            else:
                continue





patternlist = []
for i in range(0,len(wholepattern)):
    if wholepattern[i] not in patternlist:
        patternlist.append(wholepattern[i])
    else:
        continue

for i in patternlist:
    print i
    for j in range(len(wholepattern)):
        if wholepattern[j] == i:
            print j
            print wholetext[j]+'\n'
    print "*"*10