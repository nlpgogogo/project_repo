
import csv,os
import random
import string
import logging
import sys
import re
import nltk
from nltk.stem.porter import *
from nltk.corpus import stopwords
from collections import Counter
import numpy as np
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import StratifiedKFold
from pprint import pprint
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
from sklearn.cross_validation import StratifiedShuffleSplit
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier,Perceptron,PassiveAggressiveClassifier,RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier,NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score
from sklearn.tree import DecisionTreeClassifier

def get_file_path(filename):
    currentdirpath = os.getcwd()
    file_path = os.path.join(os.getcwd(), filename)
    return file_path

def read_csv(filepath,i):
    name = []
    with open(filepath, "rb") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            if len(row) == 0:
                continue
            name.append(row[i])
    return name
filename = "/Users/zhongyi/Documents/python/luo/Annotations1.csv"

path = get_file_path(filename)

print path


#read csv and put all text into list
new_text = read_csv(path, 1)#list
new_algo = read_csv(filename,2)
new_site = read_csv(filename,3)
whole = []
for i in range(4,12):
    whole.append(read_csv(filename, i))
print len(whole)

def attach(feature,list):
    index = [i for i, j in enumerate(list) if j == '1']
    print len(index)
    text = [feature[i] for i in index] 
    return text

name = ["Medication Details","Confirm Disease Was Checked","Rule of N","Use Distinct Dates",
"Level of Evidence","Credentials of the Actor","Where Did It Happen?","Check For Negation"]
whole_new_text = []
whole_new_pattern = []
whole_new_site = []
whole_new_algo = []
for i in range(8):
    temp_text= attach(new_text,whole[i])
    temp_site = attach(new_site,whole[i])
    temp_algo = attach(new_algo,whole[i])
    temp_name = [name[i]]*len(temp_text)
    whole_new_text = whole_new_text+temp_text
    whole_new_algo = whole_new_algo+temp_algo
    whole_new_site = whole_new_site+temp_site
    whole_new_pattern = whole_new_pattern+temp_name


print whole_new_text
"""
medication_details = read_csv(filename, 4)#list
whole = whole.append()
confirm = read_csv(path, 5)
rules_of_n = read_csv(path, 6)
use_distinct_dates = read_csv(path, 7)
level_of_evidence = read_csv(path, 8)
credentails = read_csv(path, 9)
where = read_csv(path, 10)
negation= read_csv(path, 11)#list
whole = []

whole = medication_details+confirm+rules_of_n+use_distinct_dates+level_of_evidence+credentails+where+negation
print len(whole)

print [i for i, j in enumerate(medication_details) if j == '1']


Text_M, pattern_M = attach(new_text,medication_details,"Medication Details")
Text_Co, pattern_Co = attach(new_text,confirm,"Confirm Disease Was Checked")
Text_R, pattern_R = attach(new_text,rules_of_n,"Rule of N")
Text_U, pattern_U = attach(new_text,use_distinct_dates,"Use Distinct Dates")
Text_L, pattern_L = attach(new_text,level_of_evidence,"Level of Evidence")
Text_Cr, pattern_Cr = attach(new_text,credentails,"Credentials of the Actor")
Text_W, pattern_W = attach(new_text,where,"Where Did It Happen")
Text_n, pattern_n= attach(new_text,negation,"Check For Negation")

new_text = Text_M+Text_Co+Text_R+Text_U+Text_L+Text_Cr+Text_W+Text_n
new_pattern = pattern_M+pattern_Co+pattern_R+pattern_U+pattern_L+pattern_Cr+pattern_W+pattern_n
print len(new_pattern)
print len(new_text)

#print allNewText
"""



















