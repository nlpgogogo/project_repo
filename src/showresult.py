import os
import csv
import random
import string
import sklearn
import logging
import sys
from collections import Counter
import numpy as np
from pprint import pprint
from sklearn import cross_validation,datasets
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier,Perceptron,PassiveAggressiveClassifier,RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier,NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score
from sklearn.tree import DecisionTreeClassifier


def get_file_path(filename):
    currentdirpath = os.getcwd()
    file_path = os.path.join(os.getcwd(), filename)
    return file_path

def read_csv(filepath,i):
    name = []
    with open(filepath, "rU") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            if len(row) == 0:
                continue
            name.append(row[i])
    return name
def prediction(clf,parameter):   
    print('_' * 100)
    print("Training: ")
    print(clf)
    pprint(parameter)
    gs_clf = GridSearchCV(clf, parameter, n_jobs=-1,scoring ="f1_micro")
    gs_clf = gs_clf.fit(X_train, all_train_pattern)
    best_parameters, score, _ = max(gs_clf.grid_scores_, key=lambda x: x[1])
    print('_' * 50)
    print("Best parameters set found on development set:")
    print(gs_clf.best_params_)
    pred = gs_clf.predict(X_test)
    print('_' * 50)
    n = 0
    for i in range(0,len(pred)):
        if pred[i] != all_test_pattern[i]: 
            n = n+1
            print "text >>>>>>>>",all_test_text[i]
            print "true pattern >>>>>>>>",all_test_pattern[i]
            print "predicted pattern >>>>>>>>",pred[i]
            print ("*"*6)
    print ("total wrong number : %d" %n)
    mirco_precisionscore = precision_score(all_test_pattern, pred,average='macro')
    micro_f1score = f1_score(all_test_pattern, pred,average='macro')
    micro_recallscore = recall_score(all_test_pattern, pred,average='macro')
    score = accuracy_score(all_test_pattern, pred)
    print('_' * 50)
    print("micro_f1_score: %f " %(micro_f1score))
    print("micro_precision_score: %f " %(mirco_precisionscore))
    print("micro_recall_score: %f " %(micro_recallscore))
    print("accuracy_score %f" %(score))
    print('_' * 50)
    print(classification_report(all_test_pattern, pred,
    target_names=pattern))
    return gs_clf




#read text file
filename = "/Users/zhongyi/Documents/python/luo/Analysis_R1R2_replaced.csv"
filename2 = "/Users/zhongyi/Documents/python/luo/CandidatePatterns.csv"
path = get_file_path(filename)
path2 = get_file_path(filename2)
print path
print path2

#read csv and put all text into list
all_text = read_csv(path, 2)#list
row_name = read_csv(path, 0)#list
Candidate_pattern= read_csv(path2, 0)#list
evidence_ID = read_csv(path2, 2)#list

#remove the pattern which has evidence less than 8
useful_pattern = []
useful_evidence = []
for j in range(0,len(evidence_ID)):
    evidence = evidence_ID[j].split(",")
    if len(evidence) > 8:
        useful_evidence.append(evidence_ID[j])
        useful_pattern.append(Candidate_pattern[j])

#match row_name with evidence
wholetext = []
wholepattern = []
for i in range(0, len(row_name)):
    for number_list in range(0,len(useful_evidence)):
        evidence = useful_evidence[number_list].split(",")
        for item in evidence:
            if str(row_name[i].strip()) == item.strip():
                wholetext.append(all_text[i])
                wholepattern.append(useful_pattern[number_list])
            else:
                continue
#make sure each pattern is 7/3 
patternlist = []
for i in range(0,len(wholepattern)):
    if wholepattern[i] not in patternlist:
        patternlist.append(wholepattern[i])
    else:
        continue

match = {}
for i in range(0,8):
    a = []
    match[patternlist[i]] = a
    for j in range(0,len(wholepattern)):
        if wholepattern[j] == patternlist[i]:
            a.append(j)
print match


random.seed(1234)
all_train_pattern = []
all_train_text = []
all_test_pattern = []
all_test_text = []
for key in match:
    lista = random.sample(match[key],int(round(0.7*len(match[key]))))
    c_list = [x for x in match[key] if x not in lista]
    for i in lista:
        all_train_pattern.append(wholepattern[i])
        all_train_text.append(wholetext[i])
    for j in c_list:
        all_test_pattern.append(wholepattern[j])
        all_test_text.append(wholetext[j])
print ("train set number")
print len(all_train_pattern)
print ("test set number")
print len(all_test_pattern)

#token
vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5,
                                 stop_words='english')#remove the stop words
X_train = vectorizer.fit_transform(all_train_text)
X_test = vectorizer.transform(all_test_text)
pattern = list(set(all_train_pattern))
print ("all patterns")
print pattern


#Classfication
np.set_printoptions(precision=3)
#svc 
C_range = list(10**x for x in range(-3,3))
gamma_range = list(10**x for x in range(-3,3))

parameters_SVC = {'kernel': ['rbf'], 
                    'gamma': gamma_range,
                     'C': C_range}

clf_svc = SVC()
prediction(clf_svc,parameters_SVC)

#linear svc
clf_linearsvc = LinearSVC(dual=False)
parameter_linearsvc = {"C" : C_range,
                       "penalty":("l1","l2")}
prediction(clf_linearsvc,parameter_linearsvc)


#random forest classifier
clf_rfc = RandomForestClassifier()
parameter_rfc = {"n_estimators":(10,100,20)}
prediction(clf_rfc,parameter_rfc)


#decision tree classifier
clf_dt = DecisionTreeClassifier()
parameter_dt = {"random_state": (0,50,20),
                "criterion" :("gini","entropy")    
}
prediction(clf_dt,parameter_dt)


##knn classifier
clf_knn = KNeighborsClassifier()
neighbors_range= range(1,21,2)
parameter_knn = {"n_neighbors" : neighbors_range}
prediction(clf_knn,parameter_knn)

#MultinomialNB
clf_nb = MultinomialNB()
parameter_nb = {"alpha" : np.arange(0.001,1,0.1)}
prediction(clf_nb,parameter_nb)
