
import csv,os
import random
import string
import logging
import sys
import re
import nltk
import pickle
from nltk.stem.porter import *
from nltk.corpus import stopwords
from collections import Counter
import numpy as np
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import StratifiedKFold
from pprint import pprint
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
from sklearn.cross_validation import StratifiedShuffleSplit
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier,Perceptron,PassiveAggressiveClassifier,RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier,NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score
from sklearn.tree import DecisionTreeClassifier

#print "with substitution for number"

def read_csv(filepath,i):
    name = []
    with open(filepath, "rb") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            if len(row) == 0:
                continue
            name.append(row[i])
    return name

def removeNonAscii(s): 
    """
    s should be a utf-8 encoded string
    """
    return "".join(i for i in s if ord(i)<128)

#find the row which is 1
def attach(feature,list):
    index = [i for i, j in enumerate(list) if j == '1']
    text = [feature[i] for i in index] 
    return text

#Lable binary    
def getPatternList(pattern, patternlist):
    for i in range(len(pattern)):
        if pattern[i] == "Medication Details":
            patternlist[0][i] = 1
        elif pattern[i] == 'Confirm Disease Was Checked':
            patternlist[1][i] = 1
        elif pattern[i] == 'Rule of N':
            patternlist[2][i] = 1
        elif pattern[i] == 'Use Distinct Dates':
            patternlist[3][i] = 1
        elif pattern[i] == 'Level of Evidence':
            patternlist[4][i] = 1
        elif pattern[i] == 'Credentials of the Actor':
            patternlist[5][i] = 1
        elif pattern[i] == 'Where Did It Happen?':
            patternlist[6][i] = 1
        elif pattern[i] == 'Check For Negation':
            patternlist[7][i] = 1
    return patternlist

#token
def delete_replication(k):
    pos = []
    neg = []
    pos_tag = []
    neg_tag = []
    for j in range(len(all_train_pattern)):
        if train_pattern[k][j] == 1:
            pos_tag.append(j)
            pos.append(all_train_text[j])
        else:
            neg_tag.append(j)
            neg.append(all_train_text[j])
    common = list(set(pos).intersection(neg))
    #print common
    #get the replicated index
    unique_index = [i for i, j in enumerate(neg) if j in common]
    unique_neg = [i for j,i in enumerate(neg_tag) if j not in unique_index]
    index = pos_tag + unique_neg
    return index
    

def token(i):
    a =  nltk.word_tokenize(i)
    words = []
    for j in a:
        words.append(stemmer.stem(j))
    return words 

def words( TEXT ):
    train = [] 
    #print TEXT  
    for si in TEXT:
        i = removeNonAscii(si)
        letters_only = []
        words = re.sub(r"(^|\s)[0-9a-z,.\-_>=<+?*/&\"\'()\[\]]*[0-9]+[0-9a-z,.\-_>=<+?*/&\"\'()\[\]]*($|\s)", " _number_ ", i)    
        words = re.sub("[/*']", " ",words)
        words = words.lower().split() 
        letters_only.append(words)
        train.append(" ".join(words))
    #print train  
    return train

def parse(file):
    codelist = []
    allcode = []
    textlist = []
    match = {}
    temp = []
    temp_stype=[]
    temp_exp = {}
    stype = []
    stype_list = []
    #exp_list = {}
    #all_exp = {}
    with open(file) as f:
        for line in f:
            if line[0:24] == "Processing 00000000.tx.1":
                temp = list(set(temp))
                #temp_exp = list(set(temp_exp))
                if temp != []:
                    codelist = codelist + temp
                    #z = temp_exp.copy()
                    #z.update(codelist)
                temp = []

                temp_stype = list(set(temp_stype))
                # print temp_stype
                if temp_stype != []:
                    stype_list = stype_list + temp_stype
                temp_stype = []

                if codelist != []:
                    allcode.append(codelist)
                    #all_exp.append(exp_list)
                if stype_list != []:
                    stype.append(stype_list)
                # print stype_list
                text = line[26:]
                textlist.append(text.rstrip())
                codelist = []
                stype_list = []
                # print "-----------------------------"
            elif line[0:6] == "Phrase":
                temp = list(set(temp))
                #temp_exp = list(set(temp_exp))
                if temp != []:
                    codelist = codelist + temp
                    #z = temp_exp.copy()
                    #z.update(codelist)
                temp = []

                temp_stype = list(set(temp_stype))
                # print temp_stype
                if temp_stype != []:
                    stype_list = stype_list + temp_stype
                temp_stype = []
            else:
                my = re.compile("C[0-9]{7}")
                # print line
                result = my.findall(line)
                if result != []:
                    temp.append(result[0])
                    a = line.index(":")
                    temp_exp[result[0]] = str(line[a+1:-1])

                my2 = re.compile("\[.+\]")
                result = my2.findall(line)
                # print line
                if result != []:
                    temp_stype.append(result[0][1:-1])
                    # print result[0]

        codelist = codelist + list(set(temp))
        # print temp_stype
        stype_list = stype_list + list(set(temp_stype))
        #exp_list = exp_list + temp_exp
        allcode.append(codelist)
        stype.append(stype_list)
        # print stype_list
        #all_exp.append(exp_list)
        #for result in re.findall("Processing(.*?)Processing",f.read(),re.S):
        #   my = re.compile("C[0-9]{7}")
        #   result = my.findall(result)
        #   codelist.append(result)
        # print stype
    return allcode, textlist, temp_exp, stype

def findOccurance(text, string):
    occ = []
    for i in text:
        a = len([m.start() for m in re.finditer(string, i)])
        occ.append(a)
    return occ
 
def umlsfeature(allcode,allwords):
    whole = []
    for i in allcode:
        freq = []
        for j in allwords:
            if j in i:
                freq.append(i.count(j))
            else:
                freq.append(0)
        whole.append(freq)
    return whole

def write(a, name):
    f = open(name,"w")
    for i in a:
        f.write(i+"\n")
    f.close()
    return f

def addmatrix(tfs_train_array,matrix,features,addfeatures,tfs_test_array,testmatrix):
    tfs_train = np.hstack((tfs_train_array,matrix)) 
    tfs_test = np.hstack((tfs_test_array, testmatrix)) 
    features = features + addfeatures 
    return tfs_train,tfs_test,features

def embedding(tfs,tfs_test,numberOcc_train,numberOcc_test):
    tfs = np.asarray(tfs.todense())
    tfs_train = np.dot(tfs, mem)#get train matrix 
    numberOcc_train = reshape(numberOcc_train)
    tfs_train = np.hstack((tfs_train,numberOcc_train)) 

    tfs_test = np.asarray(tfs_test.todense())
    tfs_test = np.dot(tfs_test, mem)
    numberOcc_test = reshape(numberOcc_test)
    tfs_test = np.hstack((tfs_test, numberOcc_test))
    #get combined feature
    features = range(200) + ["number"]  
    return tfs_train,tfs_test,features

def makeSiteMatrix(wholesite,site_category):
    site_matrix = []
    for site in wholesite:
        row = []
        for c in site_category:
            if c == site:
                row.append(1)
            else:
                row.append(0)
        site_matrix.append(row)
    return site_matrix

def makeBinaryMatrix(train_umls):
    binary_matrix = []
    for i in range(len(train_umls)):
        row = []
        for j in range(len(train_umls[i])):
            if train_umls[i][j] != 0:
                row.append(1)
            else:
                row.append(0)
        binary_matrix.append(row)
    return binary_matrix

def getunion(umls):
    l = []
    for i in umls:
        l = list(set(l+i))
    return l

def readtensor(filename):
    F = open(filename,"r")
    rank_10 = []
    for line in F:
        line = line.rstrip()
        ss = line.split(" ")
        ss = [float(i) for i in ss]
        rank_10.append(ss)
        #rank_10 = np.asarray(rank_10)
    return rank_10




#read text file
filename = "./Analysis_R1R2_replaced.csv"
filename2 = "./CandidatePatterns.csv"
filename3 = "./Annotations1.csv"#new annotation

######################################
#read new annotation
######################################

#read csv and put all text into list
new_text = read_csv(filename3, 1)#list
new_algo = read_csv(filename3,2)
new_site = read_csv(filename3,3)
whole = []
for i in range(4,12):
    whole.append(read_csv(filename3, i))

name = ["Medication Details","Confirm Disease Was Checked","Rule of N","Use Distinct Dates",
"Level of Evidence","Credentials of the Actor","Where Did It Happen?","Check For Negation"]

whole_new_text = []
whole_new_pattern = []
whole_new_site = []
whole_new_algo = []
for i in range(8):
    temp_text= attach(new_text,whole[i])
    temp_site = attach(new_site,whole[i])
    temp_algo = attach(new_algo,whole[i])
    temp_name = [name[i]]*len(temp_text)
    whole_new_text = whole_new_text+temp_text
    whole_new_algo = whole_new_algo+temp_algo
    whole_new_site = whole_new_site+temp_site
    whole_new_pattern = whole_new_pattern+temp_name

#####################################
#read old annotation
####################################

#read csv and put all text into list
all_text = read_csv(filename, 2)#list
row_name = read_csv(filename, 0)#list
Candidate_pattern= read_csv(filename2, 0)#list
evidence_ID = read_csv(filename2, 2)#list
site = read_csv(filename, 5)
algorithm = read_csv(filename, 4)

#remove the pattern which has evidence less than 8
useful_pattern = []
useful_evidence = []
for j in range(0,len(evidence_ID)):
    evidence = evidence_ID[j].split(",")
    if len(evidence) > 7:
        useful_evidence.append(evidence_ID[j])
        useful_pattern.append(Candidate_pattern[j])

#match row_name with evidence
wholetext = []
wholepattern = []
wholesite = []
wholealgo = []
for i in range(0, len(row_name)):
    for number_list in range(0,len(useful_evidence)):
        evidence = useful_evidence[number_list].split(",")
        for item in evidence:
            if str(row_name[i].strip()) == item.strip():
                wholetext.append(all_text[i])
                wholesite.append(site[i])
                wholealgo.append(algorithm[i])
                wholepattern.append(useful_pattern[number_list])
            else:
                continue


############################################
#combine old and new together
############################################
wholetext =  wholetext + whole_new_text
wholepattern = wholepattern + whole_new_pattern
wholesite = wholesite + whole_new_site
wholealgo = wholealgo + whole_new_algo

text = []
for sentence in wholetext:
    text.append(removeNonAscii(sentence))

sss = StratifiedShuffleSplit(wholepattern, 1, test_size=0.3, random_state=0)

for train_index, test_index in sss:
    train_index = train_index.tolist()
    test_index = test_index.tolist()
    all_train_text = [text[i] for i in train_index]
    all_train_pattern = [wholepattern[i] for i in train_index]
    all_train_algo = [wholealgo[i] for i in train_index]


    all_train_site = [wholesite[i] for i in train_index]    
    all_test_text = [text[i] for i in test_index]
    all_test_pattern = [wholepattern[i] for i in test_index]
    all_test_algo = [wholealgo[i] for i in test_index]
    all_test_site = [wholesite[i] for i in test_index]

#########################
#create matrix for site and algorithm
########################

print "whole size", len(wholetext)
site_category = list(set(wholesite))
algo_category = list(set(wholealgo))
patternlist = list(set(wholepattern))

all_train_site = makeSiteMatrix(all_train_site,site_category)
testsite = makeSiteMatrix(all_test_site,site_category)
all_train_algo = makeSiteMatrix(all_train_algo,algo_category)
testalgo = makeSiteMatrix(all_test_algo,algo_category)

#########################
#create binary label for the pattern
#########################

#create binary label 
y_train = [[0 for x in range(len(all_train_pattern))] for x in range(8)]
y_test = [[0 for x in range(len(all_test_pattern))] for x in range(8)]
train_pattern = getPatternList(all_train_pattern,y_train)
test_pattern = getPatternList(all_test_pattern,y_test)
test_ = words(all_test_text)

########################################
#read tensor data
########################################

tensor_train = ["train_binary_cp10.txt","train_binary_cp20.txt","train_binary_cp30.txt","train_binary_cp40.txt",
"train_binary_cp50.txt",
"train_binary_cp60.txt",
"train_binary_cp70.txt","train_binary_cp80.txt","train_binary_cp90.txt","train_binary_cp100.txt"]

tensor_test = ["test_binary_cp10.txt","test_binary_cp20.txt","test_binary_cp30.txt","test_binary_cp40.txt",
"test_binary_cp50.txt","test_binary_cp60.txt","test_binary_cp70.txt","test_binary_cp80.txt","test_binary_cp90.txt",
"test_binary_cp100.txt"]


###############
#second
#################
tensor_train_tfidf = ["train_cp10.txt","train_cp20.txt","train_cp30.txt","train_cp40.txt","train_cp50.txt",
"train_cp60.txt","train_cp70.txt","train_cp80.txt","train_cp90.txt","train_cp100.txt"]
tensor_test_tfidf = ["test_cp10.txt","test_cp20.txt","test_cp30.txt","test_cp40.txt","test_cp50.txt",
"test_cp60.txt","test_cp70.txt","test_cp80.txt","test_cp90.txt","test_cp100.txt"]


stop = []
f = open("stoplist.txt")
stop = f.read().splitlines() 

stemmer = PorterStemmer()
countvectorizer = CountVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore",binary = True)

#fit and train
clf_linearsvc = LinearSVC(dual=False, class_weight="balanced",random_state = 0)
C_range = list(10**x for x in range(-3,3))
parameter_linearsvc = {"C" : C_range,
                        "penalty":["l2","l1"]}

def prediction(i, choice):
    index = delete_replication(i)
    trainpattern = [train_pattern[i][j] for j in index]
    traintext = [all_train_text[j] for j in index]
    trainsite = [all_train_site[j] for j in index]
    trainalgo = [all_train_algo[j] for j in index]
    #trainumls = [train_umls[j] for j in index]
    #trainst = [train_st[j] for j in index]
    train = words(traintext)#substitute number
    #print train
    #change the training and testing matrix to tfidf
    tfs = countvectorizer.fit_transform(train)
    tfs_test = countvectorizer.transform(test_)

    numberOcc_train = findOccurance(train,"_number_") #find _number_ occurance in each sentences        
    numberOcc_test = findOccurance(test_,"_number_")
    #get the feature of bag of words
    features_words = countvectorizer.get_feature_names()
    #print features_words
    tfs_train_array = tfs.toarray()
    tfs_test_array = tfs_test.toarray() 
    if choice == "tensor":
        tfs_train = rank
        tfs_test = rank_test
        features = range(len(rank[0]))
    if choice == "st_only":
        features = st
        tfs_train = trainst
        tfs_test = testst
    if choice == "none":   
        features = features_words
        tfs_train = tfs
        tfs_test = tfs_test
    if choice == "embedding":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
    if choice == "site":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainsite,features_words,site_category,tfs_test_array,testsite)
    if choice == "algorithm":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainalgo,features_words,algo_category,tfs_test_array,testalgo)
    if choice == "umls":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainumls,features_words,feature_umsl,tfs_test_array,test_umls)
    if choice == "st":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainst,features_words,st,tfs_test_array,test_st)
    if choice == "embedding+site":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainsite,features_words,site_category,tfs_test,testsite)
    if choice == "embedding+algo":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainalgo,features_words,site_category,tfs_test,testalgo)
    if choice == "embedding+umls":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainumls,features,feature_umsl,tfs_test,test_umls)
    if choice == "embedding+st":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainst,features,st,tfs_test,test_st)
    if choice == "embedding+tfidf":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,tfs_train_array,features,features_words,tfs_test,tfs_test_array)
    #normalization after all transformation
    #print('*' * 60)
    tfs_train = preprocessing.normalize(tfs_train, norm='l2') 
    #print tfs_train.shape
    print "cp rank:", len(tfs_train[1])
    #print name[i], "classfication"
    #print ("training size: %d" %len(trainpattern)) 
    print "option:", choice
    #print('*' * 60)
    #print("tunning parameter")+"\n"
    #print("tuning parameter grid: ")
    #pprint(parameter_linearsvc)
    #print("\n")
    #tunning prarmeter
    print train_pattern
    cv = StratifiedKFold(trainpattern, 3, random_state=0)
    gs_clf = GridSearchCV(clf_linearsvc, parameter_linearsvc, n_jobs=-1,scoring ="roc_auc", cv = cv)
    gs_clf = gs_clf.fit(tfs_train, trainpattern)
    #print("roc_auc score grid for parameter tuning:")
    #print("\n")
    #print gs_clf.grid_scores_
    #print("\n")
    #print("Best parameters set found on development set:")
    best_estimator = gs_clf.best_estimator_
    #print(best_estimator)
    #print('_' * 50)
    """
    print "cross validation report:"
    print("\n")
    auc_list = []
    #fit with all train data using best estimator    
    for train_index, test_index in cv:
        a = np.array(trainpattern)
        text_train, text_test = tfs_train[train_index], tfs_train[test_index]
        y_train, y_test = a[train_index], a[test_index]
        #fit each fold and predict
        c = best_estimator.fit(text_train, y_train)
        pred = c.predict(text_test)
        #print pred
        #print y_test
        y_test = y_test.tolist()
        test = [s for s, j in enumerate(y_test) if j == 1]
        for s in test:
            test_sen = test_index[s] 
            print traintext[test_sen]
            print "prediction:",pred[s]
        probas_ = c.decision_function(text_test)
        # Compute ROC curve and area the curve
        fpr, tpr, thresholds = roc_curve(y_test, probas_)
        #print fpr,tpr
        print fpr,tpr
        roc_auc = auc(fpr, tpr) 
        auc_list.append(roc_auc) 
    print("\n")
    print "auc score for each fold of corss validation:",auc_list
    """
    print ("best roc_auc score:"),gs_clf.best_score_
    #print('_' * 50)
    #print("training model")
    clf = best_estimator.fit(tfs_train,trainpattern)
    #word = {}
    #coeff = clf.coef_
    #for k in range(0,len(coeff[0])):
    #    if coeff[0][k] != 0:
    #        word[features[k]] = coeff[0][k]
    
    #print("\n")
    #print "rank feature in decreasing order:"
    #print sorted(word.items(), key=lambda x: x[1], reverse=True)[1:10]
    #print "rank feature in increasing order:"
    #print sorted(word.items(), key=lambda x: x[1])[1:10]
    #predict
    #print('_' * 50)
    #print("prediction")
    #print mem_te.shape
    probas_ = clf.decision_function(tfs_test)
    #print probas_
    fpr, tpr, thresholds = roc_curve(test_pattern[i], probas_)
    roc_auc = auc(fpr, tpr) 
    print("roc_auc of prediction: %f" %roc_auc)    
    pred = clf.predict(tfs_test)
    print(classification_report(test_pattern[i], pred))
    print test_pattern[i]
    #print ('\n'*6)

    return best_estimator   

"""
for i in range(len(tensor_train)):
    rank = readtensor(tensor_train[i])
    rank_test = readtensor(tensor_test[i])
    prediction(0,"tensor")

"""
for i in range(len(tensor_train_tfidf)):
    rank = readtensor(tensor_train_tfidf[i])
    rank_test = readtensor(tensor_test_tfidf[i])
    prediction(0,"tensor")   


