import csv,os
import random
import string
import logging
import sys
import re
import glob
import nltk
import pickle
from sklearn.cluster import KMeans
from nltk.stem.porter import *
from nltk.corpus import stopwords
from collections import Counter
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc,f1_score,adjusted_rand_score
from sklearn.cross_validation import StratifiedKFold
from pprint import pprint
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
from sklearn.cross_validation import StratifiedShuffleSplit
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.svm import LinearSVC,SVC
from sklearn.utils.extmath import density
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score



def read_csv(filepath,i):
    name = []
    with open(filepath, "rb") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            if len(row) == 0:
                continue
            name.append(row[i])
    return name

#find the row which is 1
def attach(feature,list):
    index = [i for i, j in enumerate(list) if j == '1']
    text = [feature[i] for i in index] 
    return text

def match_feature(whole_text,traintext,testtext,feature):
	train = []
	test = []
	#print traintext
	for i in traintext:
		train.append(feature[whole_text.index(i)])
	for i in testtext:
		test.append(feature[whole_text.index(i)])
	return train,test

def makeSiteMatrix(wholesite,site_category):
    site_matrix = []
    for site in wholesite:
        row = []
        for c in site_category:
            if c == site:
                row.append(1)
            else:
                row.append(0)
        site_matrix.append(row)
    return site_matrix

def removeNonAscii(s): 
    """
    s should be a utf-8 encoded string
    """
    return "".join(i for i in s if ord(i)<128)

def token(i):
    a =  nltk.word_tokenize(i)
    words = []
    for j in a:
        words.append(stemmer.stem(j))
    return words 

def words( TEXT ):
    train = [] 
    #print TEXT  
    for si in TEXT:
        i = removeNonAscii(si)
        i = i.lower()
        words = re.sub("[/*'-]", " ",i)
        words = re.sub(r"(^|\s)[0-9a-z,.\-_>=<+?*/&\"\'()\[\]]*[0-9]+[0-9a-z,.\-_>=<+?*/&\"\'()\[\]]*($|\s)", " _number_ ", words)    
        words = re.sub(r"\b(one|two|three|four)\b","_number_",words)
        train.append(words)
    return train

def binary(values,l,name):
    for ss in range(len(values)):
        if name in values[ss]:
            l.append(1)
        else:
            l.append(0)
    return l

def addmatrix(tfs_train_array,matrix,features,addfeatures,tfs_test_array,testmatrix):
    matrix = preprocessing.normalize(matrix, norm='l2')
    testmatrix = preprocessing.normalize(testmatrix, norm='l2')
    tfs_train = np.hstack((tfs_train_array,matrix)) 
    tfs_test = np.hstack((tfs_test_array, testmatrix)) 
    features = features + addfeatures 
    return tfs_train,tfs_test,features

def bag_of_words(train_text,test_text):
    tfs = countvectorizer.fit_transform(train_text)
    tfs_test = countvectorizer.transform(test_text)
    features_words = countvectorizer.get_feature_names()
    tfs = preprocessing.normalize(tfs, norm='l2') 
    tfs_test = preprocessing.normalize(tfs_test, norm='l2') 
    tfs = tfs.toarray()
    tfs_test = tfs_test.toarray()
    return tfs, tfs_test,features_words

def parse(file):
    codelist = []
    allcode = []
    textlist = []
    match = {}
    temp = []
    temp_stype=[]
    temp_exp = {}
    stype = []
    stype_list = []
    with open(file) as f:
        for line in f:
            if line[0:24] == "Processing 00000000.tx.1":
                temp = list(set(temp))
                if temp != []:
                    codelist = codelist + temp                   
                temp = []
                temp_stype = list(set(temp_stype))
                # print temp_stype
                if temp_stype != []:
                    stype_list = stype_list + temp_stype
                temp_stype = []

                if codelist != []:
                    allcode.append(codelist)
                    #all_exp.append(exp_list)
                if stype_list != []:
                    stype.append(stype_list)
                # print stype_list
                text = line[26:]
                textlist.append(text.rstrip())
                codelist = []
                stype_list = []
                # print "-----------------------------"
            elif line[0:6] == "Phrase":
                temp = list(set(temp))
                #temp_exp = list(set(temp_exp))
                if temp != []:
                    codelist = codelist + temp
                    #z = temp_exp.copy()
                    #z.update(codelist)
                temp = []

                temp_stype = list(set(temp_stype))
                # print temp_stype
                if temp_stype != []:
                    stype_list = stype_list + temp_stype
                temp_stype = []
            else:
                my = re.compile("C[0-9]{7}")
                # print line
                result = my.findall(line)
                if result != []:
                    temp.append(result[0])
                    a = line.index(":")
                    temp_exp[result[0]] = str(line[a+1:-1])

                my2 = re.compile("\[.+\]")
                result = my2.findall(line)
                # print line
                if result != []:
                    temp_stype.append(result[0][1:-1])
                    # print result[0]

        codelist = codelist + list(set(temp))
        # print temp_stype
        stype_list = stype_list + list(set(temp_stype))
        allcode.append(codelist)
        stype.append(stype_list)
    return allcode, textlist, temp_exp, stype

#union of all codes
def getunion(umls):
    l = []
    for i in umls:
        l = list(set(l+i))
    return l

def umlsfeature(allcode,allwords):
    whole = []
    for i in allcode:
        freq = []
        for j in allwords:
            if j in i:
                freq.append(i.count(j))
            else:
                freq.append(0)
        whole.append(freq)
    whole = np.array(whole)
    return whole

def findOccurance(text, string):
    occ = []
    for i in text:
        a = len([m.start() for m in re.finditer(string, i)])
        occ.append(a)
    return occ

def reshape(x):
    x1, x_cate = np.unique(x,return_inverse = True)
    x_cate = x_cate.reshape(len(x),1)
    return x_cate


def embedding(tfs,tfs_test,numberOcc_train,numberOcc_test,mem):
    tfs_train = np.dot(tfs, mem)#get train matrix 
    numberOcc_train = reshape(numberOcc_train)
    tfs_train = np.hstack((tfs_train,numberOcc_train)) 

    tfs_test = np.dot(tfs_test, mem)
    numberOcc_test = reshape(numberOcc_test)
    tfs_test = np.hstack((tfs_test, numberOcc_test))
    tfs_train = preprocessing.normalize(tfs_train, norm='l2')
    tfs_test = preprocessing.normalize(tfs_test, norm='l2')
    #get combined feature
    features = range(200) + ["number"]  
    return tfs_train,tfs_test,features

def print_feature(i):
    print name[i]
    print "*"*20
    a = [x for x,y in enumerate(patternlist_train[i]) if y == 1]
    for aa in a:
        print all_train_text[aa]
        print "baseline"
        print x_train[aa].tolist()
        print "baseline+site"
        print x_train_site[aa].tolist()
        print "baseline+algo"
        print x_train_algo[aa].tolist()
        print "baseline+CUI"
        print x_train_CUI[aa].tolist()
        print "baseline+st"
        print x_train_st[aa].tolist()
"""
        print "embedding"
        print x_train_embedding[aa]
        print "embedding+site"
        print x_train_em_site[aa]
        print "embedding+algo"
        print x_train_em_algo[aa]
        print "embedding+CUI"
        print x_train_em_cui[aa]
        print "embedding+st"
        print x_train_em_st[aa]
"""
def subset(wordlist1,wordlist2):
    subset_embedding = []
    for ss in wordlist2:
        subset_embedding.append(mem[wordlist1.index(ss)])
    subset_embedding = np.asarray(subset_embedding)
    return subset_embedding


def makeBinaryMatrix(train_umls):
    binary_matrix = []
    for i in range(len(train_umls)):
        row = []
        for j in range(len(train_umls[i])):
            if train_umls[i][j] != 0:
                row.append(1)
            else:
                row.append(0)
        binary_matrix.append(row)
    return binary_matrix
    
def readtensor(filename):
    F = open(filename,"r")
    rank = []
    for line in F:
        line = line.rstrip()
        ss = line.split(" ")
        ss = [float(i) for i in ss]
        rank.append(ss)
        #rank_10 = np.asarray(rank_10)
    return rank



def prediction(x_train,x_test,y_train,y_test,features):   
    #tunning prarmeter
    cv = StratifiedKFold(y_train, 3, random_state=0)
    gs_clf = GridSearchCV(clf_linearsvc, parameter_linearsvc, n_jobs=-1,scoring ="roc_auc", cv = cv)
    gs_clf = gs_clf.fit(x_train, y_train)
    #print ("best roc_auc score for cross_validation:"),gs_clf.best_score_ 
    
    #print("Best parameters set found on development set:")
    best_estimator = gs_clf.best_estimator_
    #print(best_estimator)       
    clf = best_estimator.fit(x_train,y_train)
    coeff = clf.coef_
    word = {}
    for k in range(len(coeff[0])):
        if coeff[0][k] != 0:
            word[features[k]] = coeff[0][k]
    #print("\n")
    #print "rank feature in decreasing order:"
    #print sorted(word.items(), key=lambda x: x[1], reverse=True)[0:20]
    probas_ = clf.decision_function(x_test)
    fpr, tpr, thresholds = roc_curve(y_test, probas_)
    roc_auc = auc(fpr, tpr) 
    #print("roc_auc of prediction: %f" %roc_auc)    
    pred = clf.predict(x_test)
    f1 = f1_score(y_test, pred, average='binary') 

    #print(classification_report(y_test, pred))
    #print('_' * 50)
    return f1


#read text file
filename = "./Analysis_R1R2_replaced.csv"
filename2 = "./CandidatePatterns.csv"
filename3 = "./Annotations1.csv"#new annotation


name = ["Medication Details","Confirm Disease Was Checked","Rule of N","Use Distinct Dates",
"Level of Evidence","Credentials of the Actor","Where Did It Happen?","Check For Negation"]

#read stop list
stop = []
f = open("stoplist3.txt")
stop = f.read().splitlines() #stop list containing have


#read csv and put all text into list
all_text = read_csv(filename, 2)#list
row_name = read_csv(filename, 0)#list
Candidate_pattern= read_csv(filename2, 0)#list
evidence_ID = read_csv(filename2, 2)#list
site = read_csv(filename, 5)
algorithm = read_csv(filename, 4)

#read csv and put all text into list
new_text = read_csv(filename3, 1)#list
new_algo = read_csv(filename3,2)
new_site = read_csv(filename3,3)
whole = []
for i in range(4,12):
    whole.append(read_csv(filename3, i))

#match the annotation file
whole_new_text = []
whole_new_pattern = []
whole_new_site = []
whole_new_algo = []
for i in range(8):
    temp_text= attach(new_text,whole[i])
    temp_site = attach(new_site,whole[i])
    temp_algo = attach(new_algo,whole[i])
    temp_name = [name[i]]*len(temp_text)
    whole_new_text = whole_new_text+temp_text
    whole_new_algo = whole_new_algo+temp_algo
    whole_new_site = whole_new_site+temp_site
    whole_new_pattern = whole_new_pattern+temp_name


#remove the pattern which has evidence less than 8
useful_pattern = []
useful_evidence = []
for j in range(0,len(evidence_ID)):
    evidence = evidence_ID[j].split(",")
    if len(evidence) > 7:
        useful_evidence.append(evidence_ID[j])
        useful_pattern.append(Candidate_pattern[j])

#match row_name with evidence
wholetext = []
wholepattern = []
wholesite = []
wholealgo = []
for i in range(0, len(row_name)):
    for number_list in range(0,len(useful_evidence)):
        evidence = useful_evidence[number_list].split(",")
        for item in evidence:
            if str(row_name[i].strip()) == item.strip():
                wholetext.append(all_text[i])
                wholesite.append(site[i])
                wholealgo.append(algorithm[i])
                wholepattern.append(useful_pattern[number_list])
            else:
                continue


#combine two file together
whole_text =  wholetext + whole_new_text
wholepattern = wholepattern + whole_new_pattern
wholesite = wholesite + whole_new_site
wholealgo = wholealgo + whole_new_algo



#get unique sentences, and match with labels in a dict
unique_text = list(set(whole_text))
match = {}
for i in range(len(unique_text)):
    match[unique_text[i]] = []
    for j in range(len(whole_text)):
        if whole_text[j] == unique_text[i]:
            match[unique_text[i]].append(wholepattern[j])


#get unique sentences
wholepattern = match.values()
text = match.keys()

patternsplit = []
for pattern in wholepattern:
	patternsplit.append(pattern[0])

#split train and test based on the first label of each sentences
sss = StratifiedShuffleSplit(patternsplit, 1, test_size=0.3, random_state=10)   
for train_index, test_index in sss:
	train_index =  train_index.tolist()
	test_index = test_index.tolist()

all_train_pattern, all_test_pattern = [wholepattern[i] for i in train_index], [wholepattern[i] for i in test_index]
all_train_text, all_test_text = [text[i] for i in train_index], [text[i] for i in test_index]


train_text = words(all_train_text)
test_text = words(all_test_text)

numberOcc_train = findOccurance(train_text,"_number_")         
numberOcc_test = findOccurance(test_text,"_number_")


print "whole dataset size",len(patternsplit)
print "train dataset size", len(all_train_text)
print "test dataset size", len(all_test_text)


#get the train and test data for umls mapping
"""
traintext = open("traintext_0412.txt","w")
for i in all_train_text:
    i = removeNonAscii(i)
    traintext.write(i+'\n')
traintext.close()

testtext = open("testtext_0412.txt","w")
for i in all_test_text:
	i = removeNonAscii(i)
	testtext.write(i+'\n')
testtext.close()
"""

#create categorical site and algorithm 
all_train_site,all_test_site = match_feature(whole_text,all_train_text,all_test_text,wholesite)
all_train_algo,all_test_algo = match_feature(whole_text,all_train_text,all_test_text,wholealgo)

site_category = list(set(wholesite))
algo_category = list(set(wholealgo))

all_train_site = makeSiteMatrix(all_train_site,site_category)
all_test_site = makeSiteMatrix(all_test_site,site_category)
all_train_algo = makeSiteMatrix(all_train_algo,algo_category)
all_test_algo = makeSiteMatrix(all_test_algo,algo_category)

#get the pattern list for each class
patternlist_train = []
patternlist_test = []
for i in range(8):
    l = []
    s = []
    a = binary(all_train_pattern,l,name[i])
    b = binary(all_test_pattern,s,name[i])
    patternlist_train.append(a)
    patternlist_test.append(b)

#read umls file
allcode_train, textlist_train,all_exp,all_st_train = parse("umls_train_0412.txt")
allcode_test,textlist_test,test_exp,all_st_test = parse("umls_test_0412.txt")

#get the union of all CUI code and st
CUI = getunion(allcode_train)
st = getunion(all_st_train)

all_train_CUI = umlsfeature(allcode_train,CUI)
all_test_CUI = umlsfeature(allcode_test,CUI)
all_train_st = umlsfeature(all_st_train,st)
all_test_st = umlsfeature(all_st_test,st)

#binary cui and st matrix for train and test
all_train_CUI_binary = makeBinaryMatrix(all_train_CUI)
all_train_st_binary = makeBinaryMatrix(all_train_st)
all_test_CUI_binary = makeBinaryMatrix(all_test_CUI)
all_test_st_binary = makeBinaryMatrix(all_test_st)

#nlp training and testing
stemmer = PorterStemmer()
countvectorizer = TfidfVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore")
#countvectorizer = TfidfVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore",min_df = 2)
#countvectorizer = TfidfVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore",ngram_range=(1,2),min_df = 2)
#countvectorizer = CountVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore",binary = True)
#countvectorizer = CountVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore",binary = True, min_df = 2)
#countvectorizer = CountVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore",binary = True, ngram_range=(1,2),min_df = 2)


#classification parameter

clf_linearsvc = LinearSVC(dual=False, class_weight="balanced",random_state = 0)
C_range = list(10**x for x in range(-5,3))
#C_range = [0.00001,0.0001,0.001,0.01,0.1,1,3,5,10,20,40,60,100,1000]
parameter_linearsvc = {"C" : C_range,
                        "penalty":["l2","l1"]}

#read embedding file
fn = '../data/wordlist_0416.pik'
f = open(fn, 'rb')
a = pickle.load(f)
(mem, hw) = a
f.close()


#match embedding
wordlist = open("wordlist_0413.txt","r")
embedding_word = wordlist.read().splitlines()



x_train, x_test,features = bag_of_words(train_text,test_text)

#get the tfidf and binary data for tensor decomposition
#np.savetxt('../data/feature_matrix_train_binary_04172016.txt', x_train)
#np.savetxt('../data/feature_matrix_test_binary_04172016.txt', x_test)
#np.savetxt('../data/feature_matrix_train_tfidf_04172016.txt', x_train)
#np.savetxt('../data/feature_matrix_test_tfidf_04172016.txt', x_test)
#np.savetxt('../data/mem_04172016.txt', mem)

"""
#tfidf
x_train_site,x_test_site,features_site = addmatrix(x_train,all_train_site,features,site_category,x_test,all_test_site)
x_train_algo,x_test_algo,features_algo = addmatrix(x_train,all_train_algo,features,algo_category,x_test,all_test_algo)
x_train_CUI,x_test_CUI,features_CUI = addmatrix(x_train,all_train_CUI,features,CUI,x_test,all_test_CUI)
x_train_st,x_test_st,features_st = addmatrix(x_train,all_train_st,features,st,x_test,all_test_st)
#x_train_embedding,x_test_embedding,feature_embedding = embedding(x_train,x_test,numberOcc_train,numberOcc_test,mem)

"""


#binary
x_train_site,x_test_site,features_site = addmatrix(x_train,all_train_site,features,site_category,x_test,all_test_site)
x_train_algo,x_test_algo,features_algo = addmatrix(x_train,all_train_algo,features,algo_category,x_test,all_test_algo)
x_train_CUI,x_test_CUI,features_CUI = addmatrix(x_train,all_train_CUI_binary,features,CUI,x_test,all_test_CUI_binary)
x_train_st,x_test_st,features_st = addmatrix(x_train,all_train_st_binary,features,st,x_test,all_test_st_binary)

"""

#embedding
subset_mem = subset(embedding_word,features)
x_train_embedding,x_test_embedding,feature_embedding = embedding(x_train,x_test,numberOcc_train,numberOcc_test,subset_mem)
x_train_em_site,x_test_em_site,feature_em_site = addmatrix(x_train_embedding,all_train_site,feature_embedding,site_category,x_test_embedding,all_test_site)
x_train_em_algo,x_test_em_algo,feature_em_algo = addmatrix(x_train_embedding,all_train_algo,feature_embedding,algo_category,x_test_embedding,all_test_algo)
x_train_em_cui, x_test_em_cui, feature_em_cui = addmatrix(x_train_embedding,all_train_CUI,feature_embedding,CUI,x_test_embedding,all_test_CUI)
x_train_em_st, x_test_em_st, feature_em_st = addmatrix(x_train_embedding,all_train_st,feature_embedding,st,x_test_embedding,all_test_st)

"""


testfile = glob.glob("../data/tensor/cp_04172016_test_tfidf*.txt")
trainfile = glob.glob("../data/tensor/cp_04172016_train_tfidf*.txt")

testfile_binary = glob.glob("../data/tensor/cp_04172016_test_binary*.txt")
trainfile_binary = glob.glob("../data/tensor/cp_04172016_train_binary*.txt")


testfile = sorted(testfile)
trainfile = sorted(trainfile)

testfile_binary = sorted(testfile_binary)
trainfile_binary = sorted(trainfile_binary)

"""
for i in range(8):
    print ("#"*30)
    print "classification:",name[i] 
    cv_score = []
    for kk in range(len(testfile_binary)):
        train_tensor = readtensor(trainfile_binary[kk])
        test_tensor = readtensor(testfile_binary[kk])
        feautres = range(len(train_tensor[0]))
        score = prediction(train_tensor,test_tensor,patternlist_train[i],patternlist_test[i],features)
        cv_score.append(score)
        if (kk+1)%10 == 0:
            print trainfile_binary[kk][18:-6]
            print max(cv_score)
            cv_score = []
"""
"""
def pred_tensor(testfile, trainfile):
    for i in range(8):
        print ("#"*30)
        print "classification:",name[i] 
        cv_score = []
        for kk in range(len(testfile)):
            train_tensor = readtensor(trainfile[kk])
            test_tensor = readtensor(testfile[kk])
            feautres = range(len(train_tensor[0]))
            score = prediction(train_tensor,test_tensor,patternlist_train[i],patternlist_test[i],features)
            cv_score.append(score)
            if (kk+1)%10 == 0:
                print trainfile[kk][18:-6]
                print max(cv_score)
                cv_score = []
        return cv_score

print pred_tensor(testfile_binary,trainfile_binary)
"""
"""

for i in range(8):
    print_feature(i)



wordlist = open("wordlist_0416.txt","w")
for i in features:
	wordlist.write(i+'\n')
wordlist.close()


for i in range(8):
    print ("#"*40)

    print "classification:",name[i] 

    print "number of training sentences:", patternlist_train[i].count(1)
    print "number of testing sentences:", patternlist_test[i].count(1)
    print "option: baseline"
    prediction(x_train,x_test,patternlist_train[i],patternlist_test[i],features)
    print "option: site"
    prediction(x_train_site,x_test_site,patternlist_train[i],patternlist_test[i],features_site)
    print "option: algorithm"
    prediction(x_train_algo,x_test_algo,patternlist_train[i],patternlist_test[i],features_algo)
    print "option: CUI"
    prediction(x_train_CUI,x_test_CUI,patternlist_train[i],patternlist_test[i],features_CUI)
    print "option: st"
    prediction(x_train_st,x_test_st,patternlist_train[i],patternlist_test[i],features_st)


    print "option: embedding"
    prediction(x_train_embedding,x_test_embedding,patternlist_train[i],patternlist_test[i],feature_embedding)
    print "option: embedding+site"
    prediction(x_train_em_site,x_test_em_site,patternlist_train[i],patternlist_test[i],feature_em_site)
    print "option: embedding+algorithm"
    prediction(x_train_em_algo,x_test_em_algo,patternlist_train[i],patternlist_test[i],feature_em_algo)
    print "option: embedding+CUI"
    prediction(x_train_em_cui,x_test_em_cui,patternlist_train[i],patternlist_test[i],feature_em_cui)
    print "option: embedding+st"
    prediction(x_train_em_st,x_test_em_st,patternlist_train[i],patternlist_test[i],feature_em_st)



"""















