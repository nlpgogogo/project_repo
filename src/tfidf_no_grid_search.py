
import csv,os
import random
import string
import logging
import sys
import re
import nltk
import pickle
from nltk.stem.porter import *
from nltk.corpus import stopwords
from collections import Counter
import numpy as np
from sklearn import preprocessing
from sklearn.metrics import roc_curve, auc
from sklearn.cross_validation import StratifiedKFold
from pprint import pprint
from sklearn.grid_search import GridSearchCV
from optparse import OptionParser
from sklearn.cross_validation import StratifiedShuffleSplit
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer,TfidfVectorizer,HashingVectorizer
from sklearn.svm import LinearSVC,SVC
from sklearn.linear_model import SGDClassifier,Perceptron,PassiveAggressiveClassifier,RidgeClassifier
from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.neighbors import KNeighborsClassifier,NearestCentroid
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils.extmath import density
from sklearn.metrics import classification_report, confusion_matrix, f1_score, precision_score, recall_score, adjusted_mutual_info_score, roc_auc_score, mutual_info_score, make_scorer, roc_curve,accuracy_score
from sklearn.tree import DecisionTreeClassifier

#print "with substitution for number"

def get_file_path(filename):
    currentdirpath = os.getcwd()
    file_path = os.path.join(os.getcwd(), filename)
    return file_path

def read_csv(filepath,i):
    name = []
    with open(filepath, "rb") as csvfile:
        reader = csv.reader(csvfile)
        next(reader, None)
        for row in reader:
            if len(row) == 0:
                continue
            name.append(row[i])
    return name

def removeNonAscii(s): 
    """
    s should be a utf-8 encoded string
    """
    return "".join(i for i in s if ord(i)<128)

#find the row which is 1
def attach(feature,list):
    index = [i for i, j in enumerate(list) if j == '1']
    text = [feature[i] for i in index] 
    return text

#Lable binary    
def getPatternList(pattern, patternlist):
    for i in range(len(pattern)):
        if pattern[i] == "Medication Details":
            patternlist[0][i] = 1
        elif pattern[i] == 'Confirm Disease Was Checked':
            patternlist[1][i] = 1
        elif pattern[i] == 'Rule of N':
            patternlist[2][i] = 1
        elif pattern[i] == 'Use Distinct Dates':
            patternlist[3][i] = 1
        elif pattern[i] == 'Level of Evidence':
            patternlist[4][i] = 1
        elif pattern[i] == 'Credentials of the Actor':
            patternlist[5][i] = 1
        elif pattern[i] == 'Where Did It Happen?':
            patternlist[6][i] = 1
        elif pattern[i] == 'Check For Negation':
            patternlist[7][i] = 1
    return patternlist

#categorize the site and algorithm
def categorize(x):
    x1, x_cate = np.unique(x,return_inverse = True)
    #x_cate = x_cate.reshape(len(x),1)
    return x_cate

def reshape(x):
    x1, x_cate = np.unique(x,return_inverse = True)
    x_cate = x_cate.reshape(len(x),1)
    return x_cate

#token
def delete_replication(k):
    pos = []
    neg = []
    pos_tag = []
    neg_tag = []
    for j in range(len(all_train_pattern)):
        if train_pattern[k][j] == 1:
            pos_tag.append(j)
            pos.append(all_train_text[j])
        else:
            neg_tag.append(j)
            neg.append(all_train_text[j])
    common = list(set(pos).intersection(neg))
    #print common
    #get the replicated index
    unique_index = [i for i, j in enumerate(neg) if j in common]
    unique_neg = [i for j,i in enumerate(neg_tag) if j not in unique_index]
    index = pos_tag + unique_neg
    return index
    """
    textmerge = [all_train_text[i] for i in pos_tag] + [all_train_text[i] for i in unique_neg]
    patternmerge = [train_pattern[k][i] for i in pos_tag] + [train_pattern[k][i] for i in unique_neg]
    sitemerge = [all_train_site[i] for i in pos_tag] + [all_train_site[i] for i in unique_neg]
    algomerge = [all_train_algo[i] for i in pos_tag] + [all_train_algo[i] for i in unique_neg]
    umlscodemerge = [train_umls[i] for i in pos_tag] + [train_umls[i] for i in unique_neg]
    sitemerge = reshape(sitemerge)    
    algomerge = reshape(algomerge)
    return pos, patternmerge, textmerge, sitemerge, algomerge, umlscodemerge
    """
def token(i):
    a =  nltk.word_tokenize(i)
    words = []
    for j in a:
        words.append(stemmer.stem(j))
    return words 

def words( TEXT ):
    train = [] 
    #print TEXT  
    for si in TEXT:
        i = removeNonAscii(si)
        letters_only = []
        words = re.sub(r"(^|\s)[0-9a-z,.\-_>=<+?*/&\"\'()\[\]]*[0-9]+[0-9a-z,.\-_>=<+?*/&\"\'()\[\]]*($|\s)", " _number_ ", i)    
        words = re.sub("[/*']", " ",words)
        #words = re.sub("\s[a-zA-Z]{1}\s", " * ",words)
        #words = re.sub("\s[a-zA-Z]{1}[.]\s"," * ",words)
        #words = re.sub("\s[a-zA-Z]{1}[.]\s"," * ",words)
        #words = re.sub("[/*']", " ",words)
        #print words
        #words = words.translate(None, string.punctuation)
        words = words.lower().split() 
        #words = i.lower().split()
        #stops = set(stopwords.words("english"))
        #meaningful_words = [w for w in words if not w in stops]  
        letters_only.append(words)
        train.append(" ".join(words))
    #print train  
    return train

def check(i):
    if i == 1:
        print "positive"
    if i == 0 :
        print "negative"

def extract_ngram(txts, voc=None, binary=True):
    tok_cnt = CountVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore")
    #tok_cnt = CountVectorizer(min_df=5, ngram_range=(1, 1), binary=binary, vocabulary=voc, stop_words='english', token_pattern=r'(?u)\b([A-Za-z]\w+|\w+[A-Za-z])\b')
    txt_term = np.asarray(tok_cnt.fit_transform(txts).todense())
    #txt_term = np.asarray(tok_cnt.fit_transform(txts))
    voc = tok_cnt.vocabulary_
    return (txt_term, voc);

def parse(file):
    codelist = []
    allcode = []
    textlist = []
    match = {}
    temp = []
    temp_stype=[]
    temp_exp = {}
    stype = []
    stype_list = []
    #exp_list = {}
    #all_exp = {}
    with open(file) as f:
        for line in f:
            if line[0:24] == "Processing 00000000.tx.1":
                temp = list(set(temp))
                #temp_exp = list(set(temp_exp))
                if temp != []:
                    codelist = codelist + temp
                    #z = temp_exp.copy()
                    #z.update(codelist)
                temp = []

                temp_stype = list(set(temp_stype))
                # print temp_stype
                if temp_stype != []:
                    stype_list = stype_list + temp_stype
                temp_stype = []

                if codelist != []:
                    allcode.append(codelist)
                    #all_exp.append(exp_list)
                if stype_list != []:
                    stype.append(stype_list)
                # print stype_list
                text = line[26:]
                textlist.append(text.rstrip())
                codelist = []
                stype_list = []
                # print "-----------------------------"
            elif line[0:6] == "Phrase":
                temp = list(set(temp))
                #temp_exp = list(set(temp_exp))
                if temp != []:
                    codelist = codelist + temp
                    #z = temp_exp.copy()
                    #z.update(codelist)
                temp = []

                temp_stype = list(set(temp_stype))
                # print temp_stype
                if temp_stype != []:
                    stype_list = stype_list + temp_stype
                temp_stype = []
            else:
                my = re.compile("C[0-9]{7}")
                # print line
                result = my.findall(line)
                if result != []:
                    temp.append(result[0])
                    a = line.index(":")
                    temp_exp[result[0]] = str(line[a+1:-1])

                my2 = re.compile("\[.+\]")
                result = my2.findall(line)
                # print line
                if result != []:
                    temp_stype.append(result[0][1:-1])
                    # print result[0]

        codelist = codelist + list(set(temp))
        # print temp_stype
        stype_list = stype_list + list(set(temp_stype))
        #exp_list = exp_list + temp_exp
        allcode.append(codelist)
        stype.append(stype_list)
        # print stype_list
        #all_exp.append(exp_list)
        #for result in re.findall("Processing(.*?)Processing",f.read(),re.S):
        #   my = re.compile("C[0-9]{7}")
        #   result = my.findall(result)
        #   codelist.append(result)
        # print stype
    return allcode, textlist, temp_exp, stype

def findOccurance(text, string):
    occ = []
    for i in text:
        a = len([m.start() for m in re.finditer(string, i)])
        occ.append(a)
    return occ
 
def umlsfeature(allcode,allwords):
    whole = []
    for i in allcode:
        freq = []
        for j in allwords:
            if j in i:
                freq.append(i.count(j))
            else:
                freq.append(0)
        whole.append(freq)
    return whole

def write(a, name):
    f = open(name,"w")
    for i in a:
        f.write(i+"\n")
    f.close()
    return f
"""
def addcolumn(tfs_train_array,trainsite,tfs_test_array,testsite,features,add):
    trainsite = reshape(trainsite)
    tfs_train = np.hstack((tfs_train_array,trainsite))  
    features = features + [add]  
    tfs_test = np.hstack((tfs_test_array, testsite))
    return tfs_train,tfs_test,features
"""
def addmatrix(tfs_train_array,matrix,features,addfeatures,tfs_test_array,testmatrix):
    #change the added matrix to tfidf
    matrix = tfidf_transformer.fit_transform(matrix)
    testmatrix = tfidf_transformer.transform(testmatrix)
    #change to array to do hstack
    testmatrix = testmatrix.toarray()
    matrix = matrix.toarray()
    tfs_train = np.hstack((tfs_train_array,matrix)) 
    tfs_test = np.hstack((tfs_test_array, testmatrix)) 
    
    #print tfs_train
    #return the combined feature
    features = features + addfeatures 
    return tfs_train,tfs_test,features

def embedding(tfs,tfs_test,numberOcc_train,numberOcc_test):
    tfs = np.asarray(tfs.todense())
    tfs_train = np.dot(tfs, mem)#get train matrix 
    numberOcc_train = reshape(numberOcc_train)
    tfs_train = np.hstack((tfs_train,numberOcc_train)) 

    tfs_test = np.asarray(tfs_test.todense())
    tfs_test = np.dot(tfs_test, mem)
    numberOcc_test = reshape(numberOcc_test)
    tfs_test = np.hstack((tfs_test, numberOcc_test))
    #get combined feature
    features = range(200) + ["number"]  
    return tfs_train,tfs_test,features

def makeSiteMatrix(wholesite,site_category):
    site_matrix = []
    for site in wholesite:
        row = []
        for c in site_category:
            if c == site:
                row.append(1)
            else:
                row.append(0)
        site_matrix.append(row)
    return site_matrix

def makeBinaryMatrix(train_umls):
    binary_matrix = []
    for i in range(len(train_umls)):
        row = []
        for j in range(len(train_umls[i])):
            if train_umls[i][j] != 0:
                row.append(1)
            else:
                row.append(0)
        binary_matrix.append(row)
    return binary_matrix

def prediction(i, choice,countvectorizer):
    index = delete_replication(i)
    trainpattern = [train_pattern[i][j] for j in index]
    traintext = [all_train_text[j] for j in index]
    trainsite = [all_train_site[j] for j in index]
    trainalgo = [all_train_algo[j] for j in index]
    trainumls = [train_umls[j] for j in index]
    trainst = [train_st[j] for j in index]
    train = words(traintext)#substitute number
    #print train
    #change the training and testing matrix to tfidf
    tfs = countvectorizer.fit_transform(train)
    tfs_test = countvectorizer.transform(test_)
    numberOcc_train = findOccurance(train,"_number_") #find _number_ occurance in each sentences        
    numberOcc_test = findOccurance(test_,"_number_")
    #get the feature of bag of words
    features_words = countvectorizer.get_feature_names()
    #print features_words
    tfs_train_array = tfs.toarray()
    tfs_test_array = tfs_test.toarray() 

    if choice == "st_only":
        features = st
        tfs_train = trainst
        tfs_test = testst
    if choice == "none":   
        features = features_words
        tfs_train = tfs
        tfs_test = tfs_test
    if choice == "embedding":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
    if choice == "site":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainsite,features_words,site_category,tfs_test_array,testsite)
    if choice == "algorithm":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainalgo,features_words,algo_category,tfs_test_array,testalgo)
    if choice == "umls":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainumls,features_words,feature_umsl,tfs_test_array,test_umls)
    if choice == "st":
        tfs_train,tfs_test,features = addmatrix(tfs_train_array,trainst,features_words,st,tfs_test_array,test_st)
    if choice == "embedding+site":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainsite,features_words,site_category,tfs_test,testsite)
    if choice == "embedding+algo":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainalgo,features_words,site_category,tfs_test,testalgo)
    if choice == "embedding+umls":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainumls,features,feature_umsl,tfs_test,test_umls)
    if choice == "embedding+st":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,trainst,features,st,tfs_test,test_st)
    if choice == "embedding+tfidf":
        tfs_train,tfs_test,features = embedding(tfs,tfs_test,numberOcc_train,numberOcc_test)
        tfs_train,tfs_test,features = addmatrix(tfs_train,tfs_train_array,features,features_words,tfs_test,tfs_test_array)
    #normalization after all transformation
    #print('*' * 60)
    tfs_train = preprocessing.normalize(tfs_train, norm='l2') 
    #print ("#"*20) 
    print "option:", choice
    #print "training feature dimension:",tfs_train.shape
    #print ("#"*20) 
    #print tfs_train[0]
    clf = clf_linearsvc.fit(tfs_train, trainpattern)
    word = {}
    coeff = clf.coef_
    for k in range(0,len(coeff[0])):
        if coeff[0][k] != 0:
            word[features[k]] = coeff[0][k]
    #print("\n")
    #print "rank feature in decreasing order:"
    #print sorted(word.items(), key=lambda x: x[1], reverse=True)
    probas_ = clf.decision_function(tfs_test)
    #print probas_
    fpr, tpr, thresholds = roc_curve(test_pattern[i], probas_)
    roc_auc = auc(fpr, tpr) 
    print("roc_auc of prediction: %f" %roc_auc)    
    pred = clf.predict(tfs_test)
    print(classification_report(test_pattern[i], pred))
    return pred


#read text file
filename = "./Analysis_R1R2_replaced.csv"
filename2 = "./CandidatePatterns.csv"
filename3 = "./Annotations1.csv"#new annotation

#read stop list
stop = []
f = open("stoplist.txt")
stop = f.read().splitlines() 

#read csv and put all text into list
all_text = read_csv(filename, 2)#list
row_name = read_csv(filename, 0)#list
Candidate_pattern= read_csv(filename2, 0)#list
evidence_ID = read_csv(filename2, 2)#list
site = read_csv(filename, 5)
algorithm = read_csv(filename, 4)

#read csv and put all text into list
new_text = read_csv(filename3, 1)#list
new_algo = read_csv(filename3,2)
new_site = read_csv(filename3,3)
whole = []
for i in range(4,12):
    whole.append(read_csv(filename3, i))


name = ["Medication Details","Confirm Disease Was Checked","Rule of N","Use Distinct Dates",
"Level of Evidence","Credentials of the Actor","Where Did It Happen?","Check For Negation"]
whole_new_text = []
whole_new_pattern = []
whole_new_site = []
whole_new_algo = []
for i in range(8):
    temp_text= attach(new_text,whole[i])
    temp_site = attach(new_site,whole[i])
    temp_algo = attach(new_algo,whole[i])
    temp_name = [name[i]]*len(temp_text)
    whole_new_text = whole_new_text+temp_text
    whole_new_algo = whole_new_algo+temp_algo
    whole_new_site = whole_new_site+temp_site
    whole_new_pattern = whole_new_pattern+temp_name


#remove the pattern which has evidence less than 8
useful_pattern = []
useful_evidence = []
for j in range(0,len(evidence_ID)):
    evidence = evidence_ID[j].split(",")
    if len(evidence) > 7:
        useful_evidence.append(evidence_ID[j])
        useful_pattern.append(Candidate_pattern[j])

#match row_name with evidence
wholetext = []
wholepattern = []
wholesite = []
wholealgo = []
for i in range(0, len(row_name)):
    for number_list in range(0,len(useful_evidence)):
        evidence = useful_evidence[number_list].split(",")
        for item in evidence:
            if str(row_name[i].strip()) == item.strip():
                wholetext.append(all_text[i])
                wholesite.append(site[i])
                wholealgo.append(algorithm[i])
                wholepattern.append(useful_pattern[number_list])
            else:
                continue

wholetext =  wholetext + whole_new_text
wholepattern = wholepattern + whole_new_pattern
wholesite = wholesite + whole_new_site
wholealgo = wholealgo + whole_new_algo

print "whole size", len(wholetext)
site_category = list(set(wholesite))
algo_category = list(set(wholealgo))
patternlist = list(set(wholepattern))

match = {}
for i in range(8):
    a = []
    match[patternlist[i]] = a
    for j in range(len(wholepattern)):
        if wholepattern[j] == patternlist[i]:
            a.append(j)

       
#split into train test
sss = StratifiedShuffleSplit(wholepattern, 1, test_size=0.3, random_state=0)
for train_index, test_index in sss:
    all_train_pattern, all_test_pattern = np.array(wholepattern)[train_index], np.array(wholepattern)[test_index]
    all_train_text, all_test_text = np.array(wholetext)[train_index], np.array(wholetext)[test_index]
    all_train_site, all_test_site = np.array(wholesite)[train_index], np.array(wholesite)[test_index]
    all_train_algo, all_test_algo = np.array(wholealgo)[train_index], np.array(wholealgo)[test_index]
print "train size:", len(all_train_text)
print "test size:", len(all_test_text)

#categorize algorithem and site 
#all_train_site = categorize(all_train_site)
#testsite = reshape(all_test_site)
#all_train_algo = categorize(all_train_algo)
#testalgo = reshape(all_test_algo)

#write train text
# traintext = open("traintext.txt","w")
# for i in all_train_text:
    # print i
    # traintext.write(i+'\n')
# traintext.close()
# print len(all_train_text)
all_train_site = makeSiteMatrix(all_train_site,site_category)
testsite = makeSiteMatrix(all_test_site,site_category)
all_train_algo = makeSiteMatrix(all_train_algo,algo_category)
testalgo = makeSiteMatrix(all_test_algo,algo_category)

#print all_train_site
#get umls code:
allcode_train, textlist_train,all_exp,all_st_train = parse("umlstrainwhole.txt")
allcode_test,textlist_test,test_exp,all_st_test = parse("umlstestwhole.txt")

#union of all codes
def getunion(umls):
    l = []
    for i in umls:
        l = list(set(l+i))
    return l

l = getunion(allcode_train)
st = getunion(all_st_train)

feature_umsl = []
for i in l:
    feature_umsl.append(all_exp[i])
print "all umls codes used:", len(l)

#match get umls feature

train_umls = umlsfeature(allcode_train,l)
test_umls = umlsfeature(allcode_test,l)
train_umls = np.array(train_umls)
test_umls = np.array(test_umls)

train_st = umlsfeature(all_st_train,st)
test_st = umlsfeature(all_st_test,st)
train_st = np.array(train_st)
testst = np.array(test_st)
print train_st.shape

train_umls = makeBinaryMatrix(train_umls)
test_umls = makeBinaryMatrix(test_umls)
train_st = makeBinaryMatrix(train_st)
test_st = makeBinaryMatrix(test_st)

#get patternprint ("all patterns")
print name

#create binary label 
y_train = [[0 for x in range(len(all_train_pattern))] for x in range(8)]
y_test = [[0 for x in range(len(all_test_pattern))] for x in range(8)]
train_pattern = getPatternList(all_train_pattern,y_train)
test_pattern = getPatternList(all_test_pattern,y_test)
test_ = words(all_test_text)


stemmer = PorterStemmer()
countvectorizer = TfidfVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore")
tfidf_transformer = TfidfTransformer()
#countvectorizer = CountVectorizer(tokenizer=token, stop_words=stop,decode_error ="ignore",binary = True)



#embedding
fn = '/Users/zhongyi/Documents/python/luo/phenopat/data/embedding.pik'
#fn = '/Users/zhongyi/Documents/python/luo/phenopat/data/embedding_binary.pik'
f = open(fn, 'rb')
a = pickle.load(f)
(mem, hw) = a
f.close()

clf_linearsvc = LinearSVC(C=1,penalty = "l1",dual=False, class_weight="balanced",random_state = 0)
"""
#fit and train
clf_linearsvc = LinearSVC(dual=False, class_weight="balanced",random_state = 0)
C_range = list(10**x for x in range(-3,3))
parameter_linearsvc = {"C" : C_range,
                        "penalty":["l2","l1"]}
"""



for i in range(8):
    #print ("*"*20)
    print "classfication:",name[i]
    #print ("*"*20)
    prediction(i,"none",countvectorizer)
    prediction(i,"site",countvectorizer)
    prediction(i,"algorithm",countvectorizer)
    prediction(i,"umls",countvectorizer)    
    prediction(i,"st",countvectorizer)
    #prediction(i,"st_only")
    prediction(i,"embedding",countvectorizer)
    prediction(i,"embedding+site",countvectorizer)
    prediction(i,"embedding+algo",countvectorizer)
    prediction(i,"embedding+umls",countvectorizer)
    prediction(i,"embedding+st",countvectorizer)
    #print ("\n"*4)















