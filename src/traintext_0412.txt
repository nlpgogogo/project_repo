2 or more unique diagnosis dates 
To define a 'definite' PAD case population
select subjects having no intraocular pressure lowering medications where route of administration is ophthalmic
Normal laboratory measurements within last draw prior to exposure to drug.
Diagnosis of MRSA was made in the outpatient setting
MI defined by ICD-9 code AND clopidogrel on med list at discharge
Note:  must screen for ulcerative colitis
there was no indicator of topical or cream in the flanking text
must have two or more visits in the last five years (of last day in record)
relevant ICD9 code for Atopic Dermatitis (691.8) in two or more in person visits, on separate calendar days.
not seen in any Neurology, Developmental Psychology or Psychology departments
>=2 occurrences of the following codes [ ICD9 CODE LIST]
has 3 simultaneous* med classes below 
patients must have 2 DISTINCT dates or more, that is not 2 diagnoses on the same day and not including null date values.
mentioned on at least 2 occasions  1 month apart (does not have to be the same med classes in each of the 2 occasions)
Count of distinct dates for in-person once encounters with aphysician
Count DM-related medications and supplies by distinct Rx date
Select from subjects having two or more open-angle glaucoma diagnoses (see 10a above) within ophthalmology/optometry 
at least one recorded urine volume production during a 12-hour shiftwill proceed to boxes
Must contain at least two Past Medical History sections and Medication lists (could substitute two non-acute clinic visits or requirement for annual physical)
Patient-specific index date for controls is the date of earliest qualifying hospital admission if qualified by the hospital admission criteria, or the earliest date of exposure to a class 2 or class 3 antibiotic
ICD-9 code (more than 2 occurrences of any of the below)
Count of distinct dates of T2DM DX
2 Abnormal SBP or 2 abnormal DBP lab tests 6 months apart
Exclude subjects that have <3 BMI measures remaining
Exclude non-clinical providers
Have 2 notes (dosing information) from the Coumadin clinic (pharmacy) or the cardiology Coumadin clinic between 3 and 12 weeks apart 
Only patients who have ever had at least 2 Serum Creatinine (SCr) values within 7 daysof one another
without any of the exclusion terms
Any of the following CPT codes for Evaluation & Management (E&M) that were submitted by a provider in an Optometry or Ophthalmology department [CPT CODE LIST]
ECG Impression must not contain evidence of heart disease concepts (see list below) 
All samples were collected on an outpatient basis
create table FivePlusDxVists
Select from subjects having at least one eye exam within the last two years if living or the last two years prior to date of death if deceased, using CPT codes
relevant ICD9 code GERD (530.81) in three or more in person visits, on separate calendar days
present or positive or other affirmative mention (unqualified by negation, uncertainty, or historical reference) for cases
2 or more ICD9 codes on separate days
affirmative mentions of C. diff infection (unqualified by negation, uncertainty, or historical reference) in progress notes
absent or negative or other negative mention for controls
Note section tagging to detect non-empty past medical history and medication sections.
Case contains more than two ICD-9 codes and at least one medication from Case criteria
When a cataract is found, search for a MedLEE attribute of certainty
Select subjects who have had an eye exam
at least one eye exam within the past two years of the reference date.  
Note: For those using NLP to define medications, we require a dose, strength, route, or frequency present with the medication name to insure that the medication represents a prescribed medication.
2 or more of the same diagnosis code present
within 72 hours of admission as an inpatient
keywords that are not negated
Any other value (or the absence of a value) is considered valid
Select subjects having two or more ocular hypertension diagnoses (see 3a above) within ophthalmology/optometry 
Smoking history as defined by packs/day x years use should, be captured 
Evidence of clopidogrel use (dose information available) within 30 days of second event.
Peak ALP >= 2x ULN?
Culture drawn in the outpatient (including emergency department) setting
Count of distinct dates of T1DM DX
take only those people with 5 or more visits with a DX of interest
For orally administered medications that may affect RBC indices, we populated a list of the commonly used medications using both generic and brand names 
on same date as at least one outpatient CPT below
where provider specialty is Ophthalmology or Optometry
Will exclude all but negated terms (e.g., exclude those with possible, probable, or asserted bundle branch blocks).  Should also exclude normalization negations like LBBB no longer present.
However, there are several oral medication that are used in patients with malignancies, post-transplantation, and in autoimmune disorders. There are no ICD-9 or CPT-4 codes that correspond to the use of such medications
2 or more consecutive INR measurements that are between 2 and 3 (measurements must be separated by at least one day)
Where the medication has an associated dose, frequency, route or strength
Select from subjects having Drusen ICD9 codes given by ophthalmologist/optometrist within ophthalmology/optometry department(s).
The code picks up all the patients who have the above mentioned terms in the progress notes as long as they are not preceded or followed by negative/no/not/absence (within 3 words for nephropathy, 2 words for congenital, apkd & sickle)
having at least 2 diagnosis codes
select count-distinct-dt(records)
Select subjects having two or more ocular hypertension diagnoses (see 3a above) within ophthalmology/optometry
determine if a negated mention of diabetic retinopathy or diabetic macular edema appear in the chart after that time
congestive heart failure (CHF): any CHF codes detected  2 times in different years
Exclude any values from blood drawn during an inpatient hospitalization, ER OR Urgent Care visit
contains more than two ICD 9 codes for Ulcerative Colitis (556*)
Patients on ACEI (List 1) with two mentions in medication records for at least 6 months
coronary heart disease (CHD): any CHD codes detected  2 times in different years;
Medications (Note: be sure to exclude topical and ophthalmic preparations)
Select subjects having two or more open-angle glaucoma diagnoses (see 10a above) within ophthalmology/optometry
At least on [sic] diagnosis code for C. diff (see above) and at least one affirmative mention of C. diff infection (unqualified by negation, uncertainty, or historical reference) in progress notes.
Select from subjects having ocular hypertension diagnoses codes given by ophthalmologist/optometrist within ophthalmology/optometry department(s). 
codes indicating the absence of diabetic retinopathy and/or macular edema may be used 
family history related terms (50 characters prior to 50 characters after)
If such use of chemotherapeutic medications was detected, all later samples were excluded. If no chemotherapy was administered during the aforementioned period of time, the patients were considered to be free of disease and later samples were included in 
Combine AnatomicalSite and Condition Mentions
Has >=4 meds simultaneous* med classes below mentioned on at least 2 occasions  1 month apart (does not have to be the same med classes in each of the 2 occasions)
if t1dm-dx-dt-cnt(pt ) == 0
Has two outpatient (if possible) measurements of SBP > 140 or DBP > 90 at least one month after meeting medication criteria while still on 3 simultaneous med classes
Consider chronicity
Has all SBP<135 and DBP < 90 one month AFTER BP meds (require at least 1 BP measurement)
Select subjects having two or more Drusen diagnoses (see 6a above) within ophthalmology/optometry (see 7a above), where the earliest and most recent diagnosis are at least fourteen days apart.
Keep HDL's that were not obtained during a hospital stay
Has diagnosis codes on 2+ separate calendar days for HIV infection (ICD 042.xx_044.xx).
relevant ICD9 code for GERD (530.81 or 530.11) in two or more in person visits, on separate calendar days
first electrocardiogram (ECG) is designated as normal and lacking an exclusion criteria
4 or more inpatient or outpatient diagnoses on different days
Select from subjects having AMD ICD9 codes given by ophthalmologist/optometrist within ophthalmology/optometry department(s). 
encounter with a provider with a specialty of Optometry or Ophthalmology
Two or more relevant ICD9 codes in two or more in-person visits, on separate calendar days
Urine volume <0.5 ml/kg/h for 6 hours.
Positive mention is defined using ConText for assigning statuses to each NLP result _ positive, probable, and negative
medical record includes two or more prescriptions for GERD -related medications 
subject has 2 or more fasting glucose tests >= 126 mg/dl on 2 different dates 
3) has at least one hospital admission with a prior exposure to antibiotics 
Inpatient laboratory tests will be excluded considering acute disease states that may alter the studied variables
Peripheral arterial disease (PAD): any PAD codes given as primary or secondary diagnose;
Select subjects having two or more ocular hypertension diagnoses (see 3a above); within ophthalmology/optometry (see 10a above).
Samples collected during an inpatient hospitalization (admit date  collection date  discharge date) should be excluded unless this sample was the only one available for a patient
Count of distinct dates of DM-related DX
where the earliest and most recent diagnosis are at least fourteen days apart
To define a 'probable' PAD case population 
Identify if subject has adrenal steroids on 2 or more unique dates
provider must be a clinical provider
Check for glucose lab performed
At least one non-negated term from the following list in a 'section of interest' from the Table B
